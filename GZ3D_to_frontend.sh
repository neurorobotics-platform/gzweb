#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cd $DIR/gz3d/utils
grunt build
echo -e "\e[34mgz3d.js and gz3d.min.js created\e[39m"
cp $HBP/gzweb/gz3d/build/gz3d.js $HBP/ExDFrontend/bower_components/gz3d-hbp/gz3d/build/
cp $HBP/gzweb/gz3d/client/js/include/*.js $HBP/ExDFrontend/bower_components/gz3d-hbp/gz3d/client/js/include/

echo -e "\e[34mFiles copied to HTTP directory and to Frontend\e[39m"
