#!/bin/bash

if [[ $# -ne 0 && $# -ne 2 ]]; then
    echo "Usage: start_gzweb [gzsever_host gzserver_port]"
    exit 1
fi

ulimit -c unlimited

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

./stop_gzweb.sh

if [ ! -d http/client ]
then
  echo "gzweb not initialized, have you run ./deploy.sh yet?"
  exit
fi


./node_modules/.bin/http-server http/client &

cd gzbridge

if [ $# -eq 0 ]; then
	./ws_server.js &
fi

if [ $# -eq 2 ]; then
	./ws_server.js $1 $2 &
fi

