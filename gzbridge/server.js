#!/usr/bin/env node

"use strict"

const debug = require('debug')('ws_server');

if (process.argv.length != 2 && process.argv.length != 4) {
    console.error('Server called with wrong parameters: usage ws_server [gzserver_host gzserver_port]')
    process.exit(1);
}

const WebSocketServer = require('websocket').server;
const http = require('http');
const gzbridge = require('./build/Release/gzbridge');

/**
 * Path from where the static site is served
 */
const staticBasePath = './../http/client';

/**
 * Port to connect websocket
 */
const wsPort = 7681;

/**
 * Array of websocket connections currently active
 */
let connections = [];

/**
 * Holds the message containing all material scripts in case there is no
 * gzserver connected
 */
let materialScriptsMessage = {};

/**
 * Whether the websocket is connected to a gzserver
 */
let isConnected = false;

// process.argv[2] is gzserver host, process.argv[3] is gzserver port.
let gzNode =
    (process.argv.length === 2) ?
    new gzbridge.GZNode() : new gzbridge.GZNode(process.argv[2], process.argv[3]);

if (gzNode.getIsGzServerConnected()) {
    gzNode.loadMaterialScripts(staticBasePath + '/assets');

    var x = parseFloat(process.env.GZBRIDGE_POSE_FILTER_DELTA_TRANSLATION || '0.001')
    gzNode.setPoseMsgFilterMinimumDistanceSquared(x);
    var x = parseFloat(process.env.GZBRIDGE_POSE_FILTER_DELTA_ROTATION || '0.001')
    gzNode.setPoseMsgFilterMinimumQuaternionSquared(x);
    var x = parseFloat(process.env.GZBRIDGE_UPDATE_EARLY_THRESHOLD || '0.02')
    gzNode.setPoseMsgFilterMinimumAge(x);

    console.log('Pose message filter parameters: ');
    console.log('  minimum seconds between successive messages: ' +
        gzNode.getPoseMsgFilterMinimumAge());
    console.log('  minimum XYZ distance squared between successive messages: ' +
        gzNode.getPoseMsgFilterMinimumDistanceSquared());
    console.log('  minimum Quaternion distance squared between successive messages:'
        + ' ' + gzNode.getPoseMsgFilterMinimumQuaternionSquared());
    console.log('--------------------------------------------------------------');
} else {
    materialScriptsMessage =
        gzNode.getMaterialScriptsMessage(staticBasePath + '/assets');
}

let httpServer = http.createServer(function (request, response) {
    console.log(new Date() + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

httpServer.listen(wsPort, function () {
    console.log(new Date() + ' gzbridge Websocket is listening on port: ' + wsPort);
});

// Start websocket server
let wsServer = new WebSocketServer({
    httpServer: httpServer,
    // You should not use autoAcceptConnections for production
    // applications, as it defeats all standard cross-origin protection
    // facilities built into the protocol and the browser.  You should
    // *always* verify the connection's origin and decide whether or not
    // to accept it.
    autoAcceptConnections: false
});

function originIsAllowed(_origin) {
    // put logic here to detect whether the specified origin is allowed.
    return true;
}

wsServer.on('request', function (request) {
    console.log(new Date() + ' New request from: ' + request.origin);

    if (!originIsAllowed(request.origin)) {
        // Make sure we only accept requests from an allowed origin
        request.reject();
        console.log((new Date()) + ' Connection from origin ' +
            request.origin + ' rejected.');
        return;
    }
    // Accept request
    let connection = request.accept(null, request.origin);

    // If gzserver is not connected just send material scripts and status
    if (!gzNode.getIsGzServerConnected()) {
        // create error status and send it
        let statusMessage =
            '{"op":"publish","topic":"~/status","msg":{"status":"error"}}';
        connection.sendUTF(statusMessage);
        // send material scripts message
        connection.sendUTF(materialScriptsMessage);
        return;
    }

    connections.push(connection);

    if (!isConnected) {
        isConnected = true;
        gzNode.setConnected(isConnected);
    }

    console.log(new Date() + ' New connection accepted from: ' + request.origin +
        ' ' + connection.remoteAddress);

    // Handle messages received from client
    connection.on('message', function (message) {
        debug("On message");

        if (message.type === 'utf8') {
            debug(new Date() + ' Received Message: ' + message.utf8Data +
                ' from ' + request.origin + ' ' + connection.remoteAddress);
            gzNode.request(message.utf8Data);
        } else if (message.type === 'binary') {
            debug(new Date() + ' Received Binary Message of ' +
                message.binaryData.length + ' bytes from ' + request.origin + ' ' +
                connection.remoteAddress);
            connection.sendBytes(message.binaryData);
        }
    });
    // Handle client disconnection
    connection.on('close', function (reasonCode, description) {
        console.log(new Date() + ' Peer ' + request.origin + ' ' +
            connection.remoteAddress + ' disconnected.');

        // remove connection from array
        let conIndex = connections.indexOf(connection);
        connections.splice(conIndex, 1);

        // if there is no connection notify server that there is no connected client
        if (connections.length === 0) {
            isConnected = false;
            gzNode.setConnected(isConnected);
        }
    });
});

// If not connected, periodically send messages
if (gzNode.getIsGzServerConnected()) {
    setInterval(function () {
        if (connections.length > 0) {
            let msgs = gzNode.getMessages();
            for (let i = 0; i < connections.length; ++i) {
                for (let j = 0; j < msgs.length; ++j) {
                    connections[i].sendUTF(msgs[j]);
                }
            }
        }
    }, 10);
}

