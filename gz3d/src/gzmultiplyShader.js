/**
 *
 * GZ3D Multiply Shader
 */

GZ3D.MultiplyShader = {
  uniforms: {
    tDiffuse: { value: null },
    tMultiply: { value: null }
  },

  vertexShader: [
    'varying vec2 vUv;',

    'void main() {',

    'vUv = uv;',
    'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',

    '}'
  ].join('\n'),

  fragmentShader: [
    'uniform sampler2D tDiffuse;',
    'uniform sampler2D tMultiply;',

    'varying vec2 vUv;',

    'void main() {',

    'vec4 texel = texture2D( tDiffuse, vUv );',
    'vec4 tmul = texture2D( tMultiply, vUv );',

    'gl_FragColor =tmul;',

    '}'
  ].join('\n')
};
