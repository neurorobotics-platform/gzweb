/**
 * HBP
 *
 * The composer is called by each view to render the final scene and apply all post-processing effects.
 * Since each view can have a different OpenGL context, the composer duplicates its shaders and buffers
 * and stores them directly in the view object.
 *
 * All composer settings are defined in "gzcomposersettings.js".
 *
 */

GZ3D.Composer = function(gz3dScene) {
  this.gz3dScene = gz3dScene;
  this.scene = gz3dScene.scene;
  this.currenSkyBoxTexture = null;
  this.currentSkyBoxID = '';
  this.loadingSkyBox = false;
  this.settingsReady = false;

  this.pbrMaterial = false;
  this.pbrTotalLoadingTextures = 0;
  this.currentMasterSettings = localStorage.getItem('GZ3DMaster3DSettingsV2');

  if (!this.currentMasterSettings) {
    this.currentMasterSettings = GZ3D.MASTER_QUALITY_MIDDLE;
  }
  this.normalizedMasterSettings = null;

  //-----------------------------------
  // Sun, lens flare

  var that = this;
  var flareColor = new THREE.Color(0xffffff);

  var textureLoader = new THREE.TextureLoader();

  var textureFlare0 = textureLoader.load('img/3denv/lens/lenstart.png');
  var textureFlare1 = textureLoader.load('img/3denv/lens/lenspoly.png');
  var textureFlare2 = textureLoader.load('img/3denv/lens/lensflare2.png');
  var textureFlare3 = textureLoader.load('img/3denv/lens/lensflare3.png');
  var textureFlare4 = textureLoader.load('img/3denv/lens/lenscircle.jpg');

  this.lensFlare = new THREE.Lensflare(
    textureFlare0,
    400,
    0.0,
    THREE.AdditiveBlending,
    flareColor
  );
  this.lensFlare.customUpdateCallback = function(object) {
    that.lensFlareUpdateCallback(object);
  };

  this.lensFlare.addElement(
    new THREE.LensflareElement(textureFlare2, 512, 0.0, THREE.AdditiveBlending)
  );
  this.lensFlare.addElement(
    new THREE.LensflareElement(
      textureFlare4,
      150,
      0.1,
      THREE.AdditiveBlending,
      new THREE.Color(0xffffff),
      0.4
    )
  );

  this.lensFlare.addElement(
    new THREE.LensflareElement(textureFlare3, 60, 0.6, THREE.AdditiveBlending)
  );
  this.lensFlare.addElement(
    new THREE.LensflareElement(textureFlare3, 70, 0.7, THREE.AdditiveBlending)
  );
  this.lensFlare.addElement(
    new THREE.LensflareElement(textureFlare3, 120, 0.9, THREE.AdditiveBlending)
  );

  this.lensFlare.addElement(
    new THREE.LensflareElement(
      textureFlare1,
      150,
      0.8,
      THREE.AdditiveBlending,
      new THREE.Color(0xffffff),
      0.2
    )
  );
  this.lensFlare.addElement(
    new THREE.LensflareElement(
      textureFlare1,
      90,
      0.6,
      THREE.AdditiveBlending,
      new THREE.Color(0xffffff),
      0.2
    )
  );

  this.lensFlare.addElement(
    new THREE.LensflareElement(textureFlare3, 70, 1.0, THREE.AdditiveBlending)
  );

  this.lensFlare.position.x = -65;
  this.lensFlare.position.y = 50;
  this.lensFlare.position.z = 40.0;
  this.scene.add(this.lensFlare);

  this.init();
};

GZ3D.Composer.prototype.init = function() {
  this.minimalRender = false; // Can be used to force a rendering without any post-processing effects
};

/**
 * Lensflare call back
 *
 */

GZ3D.Composer.prototype.lensFlareUpdateCallback = function(object) {
  // This function is called by the lens flare object to set the on-screen position of the lens flare particles.

  var dist = Math.sqrt(
    object.positionScreen.x * object.positionScreen.x +
      object.positionScreen.y * object.positionScreen.y
  );

  if (dist > 1.0) {
    dist = 1.0;
  }
  dist = 1.0 - dist;

  var f,
    fl = object.lensFlares.length;
  var flare;
  var vecX = -object.positionScreen.x * 2;
  var vecY = -object.positionScreen.y * 2;

  for (f = 0; f < fl; f++) {
    flare = object.lensFlares[f];
    flare.x = object.positionScreen.x + vecX * flare.distance;
    flare.y = object.positionScreen.y + vecY * flare.distance;
    flare.rotation = 0;
  }

  object.lensFlares[0].size = 10.0 + (600.0 - 10.0) * dist;
  object.lensFlares[1].size = 512.0 + (600.0 - 512.0) * dist;
  object.lensFlares[1].rotation = dist * 0.5;
  object.lensFlares[1].opacity = dist * dist;
  object.lensFlares[2].size = 512.0 + (600.0 - 512.0) * dist;
  object.lensFlares[2].opacity = dist;
  object.lensFlares[3].size = 100.0 + (200.0 - 100.0) * dist;
};

/**
 * Init composer for a specific view
 *
 */

GZ3D.Composer.prototype.initView = function(view) {
  var width = view.canvas.width;
  var height = view.canvas.height;
  var camera = view.camera;

  //---------------------------------
  // Init the effect composer which handles all post-processing passes.

  view.composer = new THREE.EffectComposer(view.renderer);

  view.directRenderPass = new THREE.RenderPass(this.scene, camera); // First pass simply render the scene
  view.directRenderPass.enabled = true;
  view.composer.addPass(view.directRenderPass);

  //-------------------------------------------------------
  // SSAO, initialize ambient occlusion pass

  // Setup depth buffer. SSAO requires a pre-rendered depth buffer.

  view.depthMaterial = new THREE.MeshDepthMaterial();
  view.depthMaterial.depthPacking = THREE.RGBADepthPacking;
  view.depthMaterial.blending = THREE.NoBlending;

  var pars = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter };
  view.depthRenderTarget = new THREE.WebGLRenderTarget(width, height, pars);

  // Setup SSAO pass which will compute and apply SSAO.

  view.ssaoPass = new THREE.ShaderPass(GZ3D.SSAOShader);
  view.ssaoPass.uniforms['tDepth'].value = view.depthRenderTarget.texture;
  view.ssaoPass.uniforms['size'].value.set(width, height);
  view.ssaoPass.uniforms['cameraNear'].value = camera.near;
  view.ssaoPass.uniforms['cameraFar'].value = camera.far;
  view.ssaoPass.uniforms['onlyAO'].value = 0;
  view.ssaoPass.uniforms['aoClamp'].value = 0.5;
  view.ssaoPass.uniforms['lumInfluence'].value = 0.9;
  view.composer.addPass(view.ssaoPass);

  //-------------------------------------------------------
  // RGB Curves

  if (!view.rgbCurvesShader) {
    view.rgbCurvesShader = new THREE.ShaderPass(GZ3D.RGBCurvesShader);
    view.rgbCurvesShader.enabled = false;
  }

  view.composer.addPass(view.rgbCurvesShader);

  //-------------------------------------------------------
  // Levels

  view.levelsShader = new THREE.ShaderPass(GZ3D.LevelsShader);
  view.levelsShader.enabled = false;
  view.levelsShader.uniforms['inBlack'].value = 0.0;
  view.levelsShader.uniforms['inGamma'].value = 1.0;
  view.levelsShader.uniforms['inWhite'].value = 1.0;

  view.levelsShader.uniforms['outBlack'].value = 0.0;
  view.levelsShader.uniforms['outWhite'].value = 1.0;

  view.composer.addPass(view.levelsShader);

  if (view.lensFlare !== undefined) {
    view.lensFlare.screenLevels = view.levelsShader;
  }

  //---------------------------------
  // Anti-aliasing

  view.fxaaShader = new THREE.ShaderPass(THREE.FXAAShader);
  view.composer.addPass(view.fxaaShader);

  //---------------------------------
  // Bloom

  view.bloomPass = new GZ3D.BloomShader(
    new THREE.Vector2(1024, 1024),
    1.5,
    0.4,
    0.85
  );
  view.composer.addPass(view.bloomPass);

  //---------------------------------
  // Copy pass, after all the effects have been applied copy the content to the screen

  var copyPass = new THREE.ShaderPass(THREE.CopyShader);
  copyPass.renderToScreen = true;
  view.composer.addPass(copyPass);
};

/**
 * Apply user camera settings
 * Reflect updates to user camera parameters in the 3D scene.
 * Must be called before a view is updated.
 */

GZ3D.Composer.prototype.applyUserCameraSettings = function() {
  var i;
  var cs = this.gz3dScene.normalizedComposerSettings;

  if (!cs.verticalFOV) {
    cs.verticalFOV = 60;
  }
  if (!cs.nearClippingDistance) {
    cs.nearClippingDistance = 0.15;
  }
  if (!cs.farClippingDistance) {
    cs.farClippingDistance = 100.0;
  }
  if (!cs.showCameraHelper) {
    cs.showCameraHelper = false;
  }

  for (i = 0; i < this.gz3dScene.viewManager.views.length; i++) {
    if (this.gz3dScene.viewManager.views[i].name === 'main_view') {
      var camera = this.gz3dScene.viewManager.views[i].camera;

      camera.fov = cs.verticalFOV;
      // These two are 'string' types when assigned from Angular.js, so force a conversion to 'float'
      camera.near = 1.0 * cs.nearClippingDistance;
      camera.far = 1.0 * cs.farClippingDistance;

      camera.updateProjectionMatrix();

      if (cs.showCameraHelper === true) {
        camera.cameraHelper.visible = true;
        camera.cameraHelper.update();
      } else {
        camera.cameraHelper.visible = false;
      }
    }
  }
};

/**
 * Update PBR Material
 *
 */

GZ3D.Composer.prototype.updatePBRMaterial = function(node) {
  var cs = this.gz3dScene.normalizedComposerSettings;
  var materialParams = {};
  var textureLoader;
  var that = this;

  if (this.pbrMaterial) {
    if (node.material.pbrMaterialDescription !== undefined) {
      if (node.pbrMeshMaterial === undefined) {
        // Need to initialize the PBR material and store both material for future use

        // Load textures

        textureLoader = new THREE.TextureLoader();

        if (!this.loadedPBRTextures) {
          this.loadedPBRTextures = {};
        }

        Object.keys(node.material.pbrMaterialDescription).forEach(function(
          maptype
        ) {
          var mappath = node.material.pbrMaterialDescription[maptype];

          if (that.currentMasterSettings !== GZ3D.MASTER_QUALITY_BEST) {
            mappath = mappath.replace('PBR', 'LOWPBR');
          }

          if (!that.loadedPBRTextures[mappath]) {
            that.loadingPBR = true;
            that.pbrTotalLoadingTextures += 1;

            materialParams[maptype] = textureLoader.load(
              mappath,
              function() {
                that.pbrTotalLoadingTextures -= 1;
                if (that.pbrTotalLoadingTextures === 0) {
                  that.gz3dScene.refresh3DViews();
                }
              },
              undefined,
              function() {
                that.pbrTotalLoadingTextures -= 1;
                that.gz3dScene.refresh3DViews();
              }
            );

            materialParams[maptype].wrapS = THREE.RepeatWrapping;
            that.loadedPBRTextures[mappath] = materialParams[maptype];
          } else {
            materialParams[maptype] = that.loadedPBRTextures[mappath];
          }

          if (maptype === 'map') {
            node.material.map = materialParams[maptype];
          }
        });

        node.pbrMeshMaterial = new THREE.MeshStandardMaterial(materialParams);
        node.pbrMeshMaterial.transparent = node.material.transparent;
        node.pbrMeshMaterial.opacity = node.material.opacity;

        if (node.pbrMeshMaterial.transparent) {
          node.pbrMeshMaterial.side = THREE.FrontSide;
          node.pbrMeshMaterial.depthWrite = false;
          node.castShadow = false;
        }

        node.pbrMeshMaterial.blending = node.material.blending;
        node.pbrMeshMaterial.blendSrc = node.material.blendSrc;
        node.pbrMeshMaterial.blendDst = node.material.blendDst;
        node.pbrMeshMaterial.blendEquation = node.material.blendEquation;
        node.stdMeshMaterial = node.material;
        node.pbrMeshMaterial.roughness = 1.0;
        node.pbrMeshMaterial.metalness = 1.0;
        node.pbrMeshMaterial.skinning = node.stdMeshMaterial.skinning;
        node.pbrMeshMaterial.aoMapIntensity = 1.0;
      }

      if (cs.dynamicEnvMap) {
        if (this.cubeCamera) {
          node.pbrMeshMaterial.envMap = this.cubeCamera.renderTarget.texture;
        }
      } else {
        node.pbrMeshMaterial.envMap = this.currenSkyBoxTexture; // Use the sky box if no custom env map texture has been defined
      }

      if (!this.loadingPBR) {
        node.material = node.pbrMeshMaterial;
        node.material.needsUpdate = true;
      }
    }
  } else {
    if (node.stdMeshMaterial !== undefined) {
      node.material = node.stdMeshMaterial;
      node.material.needsUpdate = true;
    }

    if (
      node.material.pbrMaterialDescription !== undefined &&
      !node.material.map
    ) {
      if (node.pbrMeshMaterial) {
        node.material.map = node.pbrMeshMaterial.map;
      } else {
        var maptype = 'map';
        if (maptype in node.material.pbrMaterialDescription) {
          var mappath = node.material.pbrMaterialDescription[maptype];

          if (this.currentMasterSettings !== GZ3D.MASTER_QUALITY_BEST) {
            mappath = mappath.replace('PBR', 'LOWPBR');
          }

          if (!this.loadedPBRTextures) {
            this.loadedPBRTextures = {};
          }

          if (!this.loadedPBRTextures[mappath]) {
            this.loadingPBR = true;
            this.pbrTotalLoadingTextures += 1;

            textureLoader = new THREE.TextureLoader();

            node.material.map = materialParams[
              maptype
            ] = textureLoader.load(
              mappath,
              function() {
                that.pbrTotalLoadingTextures -= 1;
                if (that.pbrTotalLoadingTextures === 0) {
                  that.gz3dScene.refresh3DViews();
                }
              },
              undefined,
              function() {
                that.pbrTotalLoadingTextures -= 1;
                that.gz3dScene.refresh3DViews();
              }
            );

            this.loadedPBRTextures[mappath] = materialParams[maptype];
          } else {
            materialParams[maptype] = this.loadedPBRTextures[mappath];
            if (maptype === 'map') {
              node.material.map = materialParams[maptype];
            }
          }
        }
      }
    }
  }
};

/**
 * Disable shadow receiving
 *
 */

GZ3D.Composer.prototype.disableShadowReceiving = function(node) {
  node.receiveShadow = false;
  node.traverse(function(subnode) {
    subnode.receiveShadow = false;
  });
};

/**
 * Apply shadow settings
 *
 */

GZ3D.Composer.prototype.applyShadowSettings = function() {
  var cs = this.gz3dScene.normalizedComposerSettings;
  var that = this;

  if (cs.shadowSettings) {
    this.scene.traverse(function(node) {
      for (var i = 0; i < cs.shadowSettings.length; i = i + 1) {
        var settings = cs.shadowSettings[i];

        if (node.name && node.name.indexOf('robot:') === 0) {
          that.disableShadowReceiving(node);
        }

        if (node.name === settings.lightName && node instanceof THREE.Light) {
          if (settings.cameraNear) {
            node.shadow.camera.near = settings.cameraNear;
          }

          if (settings.cameraFOV) {
            node.shadow.camera.fov = 2 * settings.cameraFOV / Math.PI * 180;
          }

          if (settings.cameraFar) {
            node.shadow.camera.far = settings.cameraFar;
          }

          if (settings.mapSize) {
            node.shadow.mapSize.width = settings.mapSize;
            node.shadow.mapSize.height = settings.mapSize;
          }

          if (settings.cameraBottom) {
            node.shadow.camera.bottom = settings.cameraBottom;
            node.shadow.camera.left = settings.cameraLeft;
            node.shadow.camera.right = settings.cameraRight;
            node.shadow.camera.top = settings.cameraTop;
          }

          if (settings.bias) {
            node.shadow.bias = settings.bias;
          }

          if (settings.radius) {
            node.shadow.radius = settings.radius;
          }
        }
      }
    });
  }
};

/**
 * Apply composer settings
 *
 */

GZ3D.Composer.prototype.applyComposerSettings = function(
  updateColorCurve,
  forcePBRUpdate,
  shadowReset
) {
  var that = this;

  this.gz3dScene.refresh3DViews();
  this.settingsReady = true;

  var cs = this.gz3dScene.composerSettings;
  if (cs.pbrMaterial === undefined) {
    cs.pbrMaterial = true;
  }

  if (cs.dynamicEnvMap === undefined) {
    cs.dynamicEnvMap = false;
  }

  if (!this.skinsInitialized) {
    this.buildSkins();
  }

  this.normalizedMasterSetting = null;
  this.updateComposerWithMasterSettings();
  this.applyUserCameraSettings();

  // Updates the composer internal data with the latest settings.

  // Pass true to updateColorCurve it the color curve has been changed. This will
  // force the composer to update its ramp texture for the color curves.

  cs = this.gz3dScene.normalizedComposerSettings;

  // Shadows

  this.applyShadowSettings();

  // call setShadowsMap only when the shadow setting is changed, not at every color update (ambient occlusion,bloom,color correction)
  if (shadowReset !== true) {
    this.gz3dScene.setShadowMaps(cs.shadows);
  }

  // Fog

  if (
    (cs.fog && this.scene.fog === null) ||
    (!cs.fog && this.scene.fog !== null)
  ) {
    // turning on/off fog requires materials to be updated

    this.scene.traverse(function(node) {
      if (node.material) {
        node.material.needsUpdate = true;
        if (node.material.materials) {
          for (var i = 0; i < node.material.materials.length; i = i + 1) {
            node.material.materials[i].needsUpdate = true;
          }
        }
      }
    });
  }

  if (cs.fog) {
    this.scene.fog = new THREE.FogExp2(cs.fogColor, cs.fogDensity);
  } else {
    this.scene.fog = null;
  }

  // Update sky box

  var path, format, urls;

  if (this.currentSkyBoxID !== cs.skyBox) {
    this.currentSkyBoxID = cs.skyBox;
    forcePBRUpdate = true;

    if (this.currentSkyBoxID === '') {
      this.currenSkyBoxTexture = null; // Plain color, no needs for a texture
    } else {
      var name = this.currentSkyBoxID;
      var filename = this.currentSkyBoxID;

      if (this.currentMasterSettings !== GZ3D.MASTER_QUALITY_BEST) {
        filename = 'LOWSKY_' + filename;
      }

      path = GZ3D.assetsPath + '/sky/' + name + '/' + filename + '-';
      format = '.jpg';
      urls = [
        path + 'px' + format,
        path + 'nx' + format,
        path + 'nz' + format,
        path + 'pz' + format,
        path + 'py' + format,
        path + 'ny' + format
      ];

      this.loadingSkyBox = true;
      this.currenSkyBoxTexture = new THREE.CubeTextureLoader().load(
        urls,
        function() {
          that.loadingSkyBox = false;
          that.gz3dScene.refresh3DViews();
        }
      );
      this.currenSkyBoxTexture.format = THREE.RGBFormat;
    }
  }

  // PBR material

  if (this.pbrMaterial !== cs.pbrMaterial || forcePBRUpdate) {
    this.pbrMaterial = cs.pbrMaterial;

    this.scene.traverse(function(node) {
      if (node.material) {
        if (forcePBRUpdate && node.stdMeshMaterial) {
          node.material = node.stdMeshMaterial;
        }

        that.updatePBRMaterial(node);
        node.material.needsUpdate = true;
      }
    });

    if (cs.pbrMaterial && cs.dynamicEnvMap) {
      if (!this.cubeCamera) {
        this.cubeCamera = new THREE.CubeCamera(1, 100000, 16);
        this.cubeCamera.position.copy(new THREE.Vector3(0, 0, 1.5));
        this.scene.add(this.cubeCamera);
      }

      this.cubeMapNeedsUpdate = true;
      this.scene.traverse(function(node) {
        if (node.material && node.pbrMeshMaterial) {
          node.pbrMeshMaterial.envMap = that.cubeCamera.renderTarget.texture;
          node.pbrMeshMaterial.needsUpdate = true;
        }
      });
    }
  }

  // Color curve

  if (updateColorCurve) {
    this.gz3dScene.viewManager.views.forEach(function(view) {
      if (!view.rgbCurvesShader) {
        view.rgbCurvesShader = new THREE.ShaderPass(GZ3D.RGBCurvesShader);
        view.rgbCurvesShader.enabled = false;
      }
      // Color curve

      if (
        (cs.rgbCurve['red'] === undefined || cs.rgbCurve['red'].length < 2) &&
        (cs.rgbCurve['green'] === undefined ||
          cs.rgbCurve['green'].length < 2) &&
        (cs.rgbCurve['blue'] === undefined || cs.rgbCurve['blue'].length < 2)
      ) {
        view.rgbCurvesShader.enabled = false; // We don't need RGB curve correction, simply disable the shader pass
      } else {
        view.rgbCurvesShader.enabled = true;

        var rgbCurves = [
          { color: 'red', curve: [] },
          { color: 'green', curve: [] },
          { color: 'blue', curve: [] }
        ];

        // Prepare the curve. Vertices need to be converted to ThreeJS vectors.

        rgbCurves.forEach(function(channel) {
          var color = channel['color'];

          if (
            cs.rgbCurve[color] === undefined ||
            cs.rgbCurve[color].length < 2
          ) {
            // The curve is empty, fill the chanel with a simple linear curve (no color correction).

            channel['curve'].push(new THREE.Vector3(0, 0, 0));
            channel['curve'].push(new THREE.Vector3(0, 1, 0));
          } else {
            // Convert to vectors

            cs.rgbCurve[color].forEach(function(vi) {
              channel['curve'].push(new THREE.Vector3(vi[0], vi[1], 0));
            });
          }
        });

        // Pass the new curve to the color correction shader.

        GZ3D.RGBCurvesShader.setupCurve(
          rgbCurves[0]['curve'],
          rgbCurves[1]['curve'],
          rgbCurves[2]['curve'],
          view.rgbCurvesShader
        );
      }
    });
  }
};

/**
 * Apply composer settings to a specific 3D model
 *
 */

GZ3D.Composer.prototype.applyComposerSettingsToModel = function(model) {
  this.updateComposerWithMasterSettings();

  var that = this;

  function updatePBRForModel(node) {
    if (node.material) {
      if (node.stdMeshMaterial) {
        node.material = node.stdMeshMaterial;
      }

      that.updatePBRMaterial(node);
      node.material.needsUpdate = true;
    }
  }

  updatePBRForModel(model);

  model.traverse(function(node) {
    updatePBRForModel(node);
  });

  this.gz3dScene.refresh3DViews();
};

/**
 * Prepare cube map render
 *
 */

GZ3D.Composer.prototype.prepareCubeMapEnvMapRender = function(done) {
  var that = this;
  var i = 0;

  if (!done) {
    this.cubeMapRenderHidden = [];
  } else {
    for (i = 0; i < this.cubeMapRenderHidden.length; i++) {
      this.cubeMapRenderHidden[i].visible = true;
    }

    this.cubeMapRenderHidden = undefined;
  }

  this.scene.traverse(function(node) {
    if (node.material) {
      if (node.pbrMeshMaterial) {
        node.pbrMeshMaterial.envMap = done
          ? that.cubeCamera.renderTarget.texture
          : null;
        node.pbrMeshMaterial.needsUpdate = true;
      } else if (!done) {
        if (node.visible) {
          node.visible = false;
          that.cubeMapRenderHidden.push(node);
        }
      }
    }
  });
};

/**
 *
 * Ask composer for scene preparation readyness. Returns null when all is ready or a message describing the progress state.
 *
 */

GZ3D.Composer.prototype.scenePreparationState = function() {
  var cs = this.gz3dScene.normalizedComposerSettings;

  if (this.pbrMaterial) {
    if (this.loadingPBR) {
      return 'Loading PBR textures';
    }

    if (cs.dynamicEnvMap) {
      // Usually done at the very end
      if (this.cubeMapNeedsUpdate) {
        return 'Generating environment map';
      }
    }
  }

  if (this.currentSkyBoxID !== '') {
    if (this.loadingSkyBox) {
      return 'Loading sky box';
    }
  }

  return null;
};

/**
 * Render the scene and apply the post-processing effects.
 *
 */

GZ3D.Composer.prototype.render = function(view) {
  var that = this;
  var cs = this.gz3dScene.normalizedComposerSettings;

  if (view.composer === undefined) {
    // No ThreeJS composer for this view, we need to initialize it.
    this.initView(view);
  }

  view.renderer.clear();

  if (!this.settingsReady) {
    return;
  }

  if (this.pbrMaterial) {
    if (this.loadingPBR && this.pbrTotalLoadingTextures === 0) {
      this.loadingPBR = false;
      this.applyComposerSettings(false, true);
      this.loadingPBR = this.pbrTotalLoadingTextures !== 0;
    }
  }

  if (
    !this.loadingPBR &&
    this.pbrMaterial &&
    !this.loadingSkyBox &&
    cs.dynamicEnvMap &&
    !this.gz3dScene.dontRebuildCubemapNow
  ) {
    if (this.cubeMapNeedsUpdate) {
      this.scene.background = this.currenSkyBoxTexture;
      this.cubeMapNeedsUpdate = false;
      this.prepareCubeMapEnvMapRender(false);
      this.cubeCamera.update(view.renderer, this.scene);
      this.prepareCubeMapEnvMapRender(true);
    }
  }

  this.gz3dScene.labelManager.onRender();
  this.updateSkin();

  var camera = view.camera;
  var width = view.container.clientWidth;
  var height = view.container.clientHeight;
  var nopostProcessing =
    cs.sun === '' &&
    !cs.ssao &&
    !cs.antiAliasing &&
    !view.rgbCurvesShader.enabled &&
    !view.levelsShader.enabled &&
    this.currentSkyBoxID === '';

  if (
    this.minimalRender ||
    nopostProcessing ||
    this.currentMasterSettings === GZ3D.MASTER_QUALITY_MINIMAL
  ) {
    // No post processing is required, directly render to the screen.
    this.lensFlare.visible = false;
    this.scene.background = null;
    view.renderer.render(this.scene, camera);
  } else {
    var pixelRatio = view.renderer.getPixelRatio();
    var newWidth = Math.floor(width / pixelRatio) || 1;
    var newHeight = Math.floor(height / pixelRatio) || 1;

    view.composer.setSize(newWidth, newHeight);

    // Update the shaders with the latest settings

    // SSAO

    view.ssaoPass.enabled = cs.ssao;
    view.ssaoPass.uniforms['onlyAO'].value = cs.ssaoDisplay ? 1.0 : 0.0;
    view.ssaoPass.uniforms['aoClamp'].value = cs.ssaoClamp;
    view.ssaoPass.uniforms['lumInfluence'].value = cs.ssaoLumInfluence;
    view.ssaoPass.uniforms['size'].value.set(width, height);
    view.ssaoPass.uniforms['cameraNear'].value = camera.near;
    view.ssaoPass.uniforms['cameraFar'].value = camera.far;

    // Bloom

    view.bloomPass.enabled = cs.bloom;
    view.bloomPass.strength = cs.bloomStrength;
    view.bloomPass.radius = cs.bloomRadius;
    view.bloomPass.threshold = cs.bloomThreshold;

    // Levels

    view.levelsShader.enabled =
      cs.levelsInBlack > 0.0 ||
      cs.levelsInGamma < 1.0 - 0.00001 ||
      cs.levelsInGamma > 1.0 + 0.00001 ||
      cs.levelsInWhite < 1.0 ||
      cs.levelsOutBlack > 0.0 ||
      cs.levelsOutWhite < 1.0;

    if (view.levelsShader.enabled) {
      view.levelsShader.uniforms['inBlack'].value = cs.levelsInBlack;
      view.levelsShader.uniforms['inGamma'].value = cs.levelsInGamma;
      view.levelsShader.uniforms['inWhite'].value = cs.levelsInWhite;
      view.levelsShader.uniforms['outBlack'].value = cs.levelsOutBlack;
      view.levelsShader.uniforms['outWhite'].value = cs.levelsOutWhite;
    }

    // Anti-aliasing

    if (cs.antiAliasing) {
      view.fxaaShader.enabled = true;
      view.fxaaShader.uniforms['resolution'].value.set(
        1.0 / width,
        1.0 / height
      );
    } else {
      view.fxaaShader.enabled = false;
    }

    // Start rendering

    if (cs.ssao) {
      // If SSAO is active, we need to render the scene to a depth buffer that will be used later
      // by the SSAO shader.

      if (
        view.depthRenderTarget.width !== width ||
        view.depthRenderTarget.height !== height
      ) {
        view.depthRenderTarget.setSize(width, height); // Resize the depth buffer if required
      }

      // Render to depth buffer

      this.scene.background = null; // Some effects should be disabled during depth buffer rendering
      this.lensFlare.visible = false;
      this.scene.overrideMaterial = view.depthMaterial;
      view.renderer.clear();
      view.renderer.setRenderTarget(view.depthRenderTarget);
      view.renderer.render(this.scene, camera); // Render to depth buffer now!
      this.scene.overrideMaterial = null;
    }

    // Render all passes

    this.lensFlare.visible = cs.sun === 'SIMPLELENSFLARE'; // Display lens flare if required
    this.scene.background = this.currenSkyBoxTexture;
    this.renderingView = view;
    view.composer.render();
    this.renderingView = null;
  }
};

/**
 * Check scene ready
 *
 */

GZ3D.Composer.prototype.checkSceneReady = function() {
  // Scene ready callback

  if (this.sceneReadyCallback) {
    var prepstate = this.scenePreparationState();
    this.sceneReadyCallback(prepstate);
    if (prepstate === null) {
      this.sceneReadyCallback = undefined;
      this.gz3dScene.refresh3DViews();
    }
  }
};

/**
 * Apply master settings to composer settings
 *
 */

GZ3D.Composer.prototype.updateComposerWithMasterSettings = function() {
  if (this.normalizedMasterSetting !== this.currentMasterSettings) {
    this.normalizedMasterSetting = this.currentMasterSettings;

    this.gz3dScene.normalizedComposerSettings = JSON.parse(
      JSON.stringify(this.gz3dScene.composerSettings)
    );

    var maxShadow = null;

    switch (this.currentMasterSettings) {
      case GZ3D.MASTER_QUALITY_BEST:
        break; // Keep the settings as they are by default

      case GZ3D.MASTER_QUALITY_MINIMAL:
        this.gz3dScene.normalizedComposerSettings.antiAliasing = false;
        this.gz3dScene.normalizedComposerSettings.rgbCurve = {
          red: [],
          green: [],
          blue: []
        };
        this.gz3dScene.normalizedComposerSettings.levelsInBlack = 0.0;
        this.gz3dScene.normalizedComposerSettings.levelsInGamma = 1.0;
        this.gz3dScene.normalizedComposerSettings.levelsInWhite = 1.0;
        this.gz3dScene.normalizedComposerSettings.levelsOutBlack = 0.0;
        this.gz3dScene.normalizedComposerSettings.levelsOutWhite = 1.0;
        this.gz3dScene.normalizedComposerSettings.shadows = false;
      /* falls through */

      case GZ3D.MASTER_QUALITY_LOW:
        this.gz3dScene.normalizedComposerSettings.skyBox = '';
        this.gz3dScene.normalizedComposerSettings.pbrMaterial = false;
        this.gz3dScene.normalizedComposerSettings.fog = false;
        this.gz3dScene.normalizedComposerSettings.sun = '';
        this.gz3dScene.normalizedComposerSettings.shadows = false;
        maxShadow = 1024;
      /* falls through */

      case GZ3D.MASTER_QUALITY_MIDDLE:
        this.gz3dScene.normalizedComposerSettings.ssaoDisplay = false;
        this.gz3dScene.normalizedComposerSettings.ssao = false;
        this.gz3dScene.normalizedComposerSettings.bloom = false;

        if (!maxShadow) {
          maxShadow = 2048;
        }

      /* falls through */
    }

    if (
      maxShadow &&
      this.gz3dScene.normalizedComposerSettings.shadowSettings &&
      this.gz3dScene.normalizedComposerSettings.shadows
    ) {
      for (
        var i = 0;
        i < this.gz3dScene.normalizedComposerSettings.shadowSettings.length;
        i = i + 1
      ) {
        var settings = this.gz3dScene.normalizedComposerSettings.shadowSettings[
          i
        ];

        if (settings.mapSize >= maxShadow) {
          settings.mapSize = maxShadow;
        }
      }
    }
  }
};

/**
 * Set master settings
 *
 */

GZ3D.Composer.prototype.setMasterSettings = function(
  masterSettings,
  applySettings
) {
  if (masterSettings !== this.currentMasterSettings) {
    if (applySettings) {
      this.currentMasterSettings = masterSettings;
      this.applyComposerSettings(true, true);
      this.normalizedMasterSetting = null;
    }
    localStorage.setItem('GZ3DMaster3DSettingsV2', masterSettings);
  }
};

/**
 * Is a specific setting supported by current master settings
 *
 */

GZ3D.Composer.prototype.isSupportedByMasterSetting = function(setting) {
  switch (this.currentMasterSettings) {
    case GZ3D.MASTER_QUALITY_MINIMAL:
      return false;

    case GZ3D.MASTER_QUALITY_LOW:
      if (setting === 'shadows') {
        return false;
      }
      if (setting === 'skyBox') {
        return false;
      }
      if (setting === 'pbrMaterial') {
        return false;
      }
      if (setting === 'dynamicEnvMap') {
        return false;
      }

    /* falls through */

    case GZ3D.MASTER_QUALITY_MIDDLE:
      if (setting === 'ssao') {
        return false;
      }
      if (setting === 'ssaoDisplay') {
        return false;
      }
      if (setting === 'bloom') {
        return false;
      }
      if (setting === 'fog') {
        return false;
      }
      if (setting === 'sun') {
        return false;
      }
      break;
  }

  return true;
};

/**
 * Manage skin visibility
 *
 */

GZ3D.Composer.prototype.setSkinVisibility = function(skinnedObject, visible) {
  var that = this;

  skinnedObject.skinMesh.visible = visible;

  skinnedObject.skinBoneMap.forEach(function(jointBonePair) {
    jointBonePair.joint.visible = !visible;
  });

  if (skinnedObject.definition.hideParentMeshWhenVisible) {
    skinnedObject.parentMesh.traverse(function(object) {
      if (object instanceof THREE.Mesh && !object._isPartOfTheSkin) {
        object.visible = !visible;
      }
    });
  }

  // For now muscle visualization does not seems to be properly linked to a specific robot/model. So
  // I use the 'all_of_them' keyword to find show/hide them.

  if ('all_of_them' in this.gz3dScene.muscleVisuzalizations) {
    this.gz3dScene.muscleVisuzalizations['all_of_them'].setVisible(!visible);
  }

  this.gz3dScene.refresh3DViews();
};

/**
 * Check is skin visible for a muscle *
 */

GZ3D.Composer.prototype.isSkinVisibleForMuscle = function(muscle) {
  // For now muscles are not associated to a robot/model name. So I just show/hide them
  // based on first skinned mesh visibility state

  if (this.skinnedObjects && this.skinnedObjects.length) {
    var skinnedObject = this.skinnedObjects[0];
    return !skinnedObject.skinMesh.visible;
  }

  return true;
};

/**
 * Build skin
 *
 */

GZ3D.Composer.prototype.buildSkin = function(skin) {
  var that = this;

  if (!skin.initialized) {
    skin.initialized = true;
    var parent = that.scene.getObjectByName(skin.parentObject, true);
    if (parent) {
      var uri = GZ3D.assetsPath + '/' + skin.mesh;
      that.gz3dScene.loadCollada(
        uri, 
        null, 
        null, 
        (dae) => {
          var mapToMesh = that.scene.getObjectByName(skin.mapTo);

          if (skin.scale)
            dae.scale.copy(new THREE.Vector3(skin.scale, skin.scale, skin.scale));

          parent.add(dae);
          that.gz3dScene.refresh3DViews();

          dae.traverse(function(object) {
            object._isPartOfTheSkin = true;
          });

          var skinBoneMap = [];
          var skinnedObject = {
            skinBoneMap: skinBoneMap,
            definition: skin,
            skinMesh: dae,
            mapToMesh: mapToMesh,
            parentMesh: parent
          };

          that.skinnedObjects.push(skinnedObject);

          dae._skinnedObject = skinnedObject;
          parent._skinnedObject = skinnedObject;
          mapToMesh._skinnedObject = skinnedObject;

          dae.traverse(function(bone) {
            if (bone instanceof THREE.Bone) {
              var r = mapToMesh.getObjectByName(skin.bonePrefix + bone.name);
              if (r) {
                skinBoneMap.push({ joint: r, bone: bone });
                that.setSkinVisibility(skinnedObject, skin.visible);

                r._skinnedObject = skinnedObject;
                bone._skinnedObject = skinnedObject;
              }
            }
          });
        }
      );
    }
  }
};

/**
 * Build all skins
 *
 */

GZ3D.Composer.prototype.buildSkins = function() {
  var cs = this.gz3dScene.composerSettings;
  var that = this;

  if (!this.skinnedObjects) this.skinnedObjects = [];

  // Skins created from the .ini file of the experiment
  if (cs.skins) cs.skins.forEach(skin => that.buildSkin(skin));

  // Skins created dynamically from the frontend
  if (this.skins) this.skins.forEach(skin => that.buildSkin(skin));

  this.skinsInitialized = true;
};

/**
 * Update skin
 *
 */

GZ3D.Composer.prototype.updateSkin = function() {
  if (!this.skinnedObjects) return;

  this.scene.updateMatrixWorld();

  for (var si in this.skinnedObjects) {
    var skinnedObject = this.skinnedObjects[si];
    var skinBoneMap = skinnedObject.skinBoneMap;

    for (var i in skinBoneMap) {
      var bone = skinBoneMap[i].bone,
        joint = skinBoneMap[i].joint;

      var invM = new THREE.Matrix4();
      invM.getInverse(bone.parent.matrixWorld);

      var p = new THREE.Vector3();
      joint.getWorldPosition(p);

      var jM = new THREE.Matrix4();
      var bp = new THREE.Vector3(),
        br = new THREE.Quaternion(),
        bs = new THREE.Vector3();

      jM.multiplyMatrices(invM, joint.matrixWorld);
      jM.decompose(bp, br, bs);

      var tx = 0,
        ty = 0,
        tz = 0;

      var boneCorrection = skinnedObject.definition.jointToBoneAngleCorrection;
      if (boneCorrection && boneCorrection[bone.name]) {
        tx = boneCorrection[bone.name][0];
        ty = boneCorrection[bone.name][1];
        tz = boneCorrection[bone.name][2];
      }

      var tq = new THREE.Quaternion();
      tq.setFromEuler(new THREE.Euler(tx, ty, tz));
      br.multiply(tq);

      bone.position.copy(bp);
      bone.quaternion.copy(br);

      bone.updateMatrixWorld();
    }
  }
};

/**
 * Add a skin mesh
 *
 */

GZ3D.Composer.prototype.addSkinMesh = function(skinMeshDefinition) {
  if (!this.skins) this.skins = [];
  this.skins.push(skinMeshDefinition);

  this.skinsInitialized = false;
};
