/**
 * Object auto align helper
 * @constructor
 */
GZ3D.AutoAlignModel = function(scene) {
  this.scene = scene;
  this.init();
  this.obj = undefined;
  this.needsAlignOnFloor = false;
  this.disableSnap = false;
};

/**
 * Constants
 */

GZ3D.AutoAlignModel.HorizontalObjectSnapDist = 0.1;
GZ3D.AutoAlignModel.AutoAlignSnapDist = 0.1;
GZ3D.AutoAlignModel.InnerBorder = 0.1;

/**
 * Initialize AutoAlignModel
 */

GZ3D.AutoAlignModel.prototype.init = function() {
  this.plane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0);
  this.ray = new THREE.Ray();
  this.obj = null;
};

/**
 * Align on the bottom of the mesh.
 *
 */

GZ3D.AutoAlignModel.prototype.alignOnMeshBase = function() {
  if (this.scene.modelManipulator.lockZAxis) {
    return;
  }

  if (!this.needsAlignOnFloor && this.obj) {
    var bbox = new THREE.Box3().setFromObject(this.obj);
    if (Math.abs(bbox.min.x) !== Infinity) {
      this.needsAlignOnFloor = false;
      var point = this.obj.position.clone();

      if (!this.disableSnap) {
        point.z = this.findBestZ(point.z - bbox.min.z);
      } else {
        point.z -= bbox.min.z;
      }

      this.scene.setPose(this.obj, point, this.obj.quaternion);
    } else {
      var that = this;

      setTimeout(function() {
        that.alignOnMeshBase();
      }, 0.1);
    }
  }
};

/**
 * Initialize UI helpers
 *
 */

GZ3D.AutoAlignModel.prototype.initUIHelpers = function() {
  if (!this.displayAlignAxes) {
    this.displayAlignAxes = [];

    for (var i = 0; i < 4; i++) {
      var axisGeometry = new THREE.Geometry();
      if (i < 2) {
        axisGeometry.vertices.push(
          new THREE.Vector3(0, -1000, 0),
          new THREE.Vector3(0, 1000, 0)
        );
      } else {
        axisGeometry.vertices.push(
          new THREE.Vector3(-1000, 0, 0),
          new THREE.Vector3(1000, 0, 0)
        );
      }

      this.displayAlignAxes.push(
        new THREE.LineSegments(
          axisGeometry,
          new THREE.LineBasicMaterial({
            opacity: 0.3,
            color: 0xffffff,
            depthTest: false,
            depthWrite: false,
            transparent: true
          })
        )
      );
      this.displayAlignAxes[i].visible = false;
      this.scene.add(this.displayAlignAxes[this.displayAlignAxes.length - 1]);
    }
  }
};

/**
 * Hide UI helpers
 *
 */

GZ3D.AutoAlignModel.prototype.hideUIHelpers = function() {
  if (this.displayAlignAxes) {
    for (var i = 0; i < this.displayAlignAxes.length; i++) {
      this.displayAlignAxes[i].visible = false;
    }
  }
};

/**
 * Return true, if an object should be ignored by snap
 *
 */

GZ3D.AutoAlignModel.prototype.autoAlignIgnoreObject = function(meshobj) {
  if (
    meshobj.name.indexOf('_lightHelper') >= 0 ||
    meshobj.name === 'grid' ||
    meshobj.name === 'boundingBox' ||
    meshobj instanceof THREE.Camera ||
    meshobj instanceof THREE.Light ||
    meshobj instanceof THREE.CameraHelper ||
    meshobj instanceof THREE.Lensflare ||
    meshobj instanceof THREE.GridHelper
  ) {
    return true;
  }

  while (meshobj) {
    if (meshobj === this.obj) {
      return true;
    }
    meshobj = meshobj.parent;
  }

  return false;
};

/**
 * Find lower Z of ray cast
 *
 */

GZ3D.AutoAlignModel.prototype.raycastToLowerZ = function(point, vector) {
  var raycaster = new THREE.Raycaster();
  var objects,
    i,
    oneFound = false,
    idesc;
  var lowerZ = 1000;
  var lowerObject;

  raycaster.set(point, vector);
  objects = raycaster.intersectObjects(this.scene.scene.children, true);

  for (i = 0; i < objects.length; i++) {
    idesc = objects[i];
    if (!this.autoAlignIgnoreObject(idesc.object)) {
      if (!oneFound || lowerZ < idesc.point.z) {
        lowerZ = idesc.point.z;
        lowerObject = idesc.object;
      }

      oneFound = true;
    }
  }

  if (!oneFound) {
    return undefined;
  }

  return { lowerZ: lowerZ, lowerObject: lowerObject };
};

/**
 * Generate a snap box aligned on a face of the bounding box.
 *
 */

GZ3D.AutoAlignModel.prototype.snapToObjectGenInnerFaceCube = function(
  bbox,
  faceIndex,
  snapsize
) {
  var innerCube = new THREE.Box3();

  innerCube.min.z =
    bbox.min.z + (bbox.max.z - bbox.min.z) * GZ3D.AutoAlignModel.InnerBorder;
  innerCube.max.z =
    bbox.max.z - (bbox.max.z - bbox.min.z) * GZ3D.AutoAlignModel.InnerBorder;

  switch (faceIndex) {
    case 0:
      innerCube.min.x = bbox.min.x - snapsize;
      innerCube.max.x = bbox.min.x + snapsize;
      innerCube.min.y =
        bbox.min.y +
        (bbox.max.y - bbox.min.y) * GZ3D.AutoAlignModel.InnerBorder;
      innerCube.max.y =
        bbox.max.y -
        (bbox.max.y - bbox.min.y) * GZ3D.AutoAlignModel.InnerBorder;
      break;

    case 1:
      innerCube.min.x = bbox.max.x - snapsize;
      innerCube.max.x = bbox.max.x + snapsize;
      innerCube.min.y =
        bbox.min.y +
        (bbox.max.y - bbox.min.y) * GZ3D.AutoAlignModel.InnerBorder;
      innerCube.max.y =
        bbox.max.y -
        (bbox.max.y - bbox.min.y) * GZ3D.AutoAlignModel.InnerBorder;
      break;

    case 2:
      innerCube.min.y = bbox.min.y - snapsize;
      innerCube.max.y = bbox.min.y + snapsize;
      innerCube.min.x =
        bbox.min.x +
        (bbox.max.x - bbox.min.x) * GZ3D.AutoAlignModel.InnerBorder;
      innerCube.max.x =
        bbox.max.x -
        (bbox.max.x - bbox.min.x) * GZ3D.AutoAlignModel.InnerBorder;
      break;

    case 3:
      innerCube.min.y = bbox.max.y - snapsize;
      innerCube.max.y = bbox.max.y + snapsize;
      innerCube.min.x =
        bbox.min.x +
        (bbox.max.x - bbox.min.x) * GZ3D.AutoAlignModel.InnerBorder;
      innerCube.max.x =
        bbox.max.x -
        (bbox.max.x - bbox.min.x) * GZ3D.AutoAlignModel.InnerBorder;
      break;
  }

  return innerCube;
};

/**
 * Compute the gravity of a face of a bounding box
 *
 */

GZ3D.AutoAlignModel.prototype.gravityPointOfFace = function(bbox, faceIndex) {
  switch (faceIndex) {
    case 0:
      return new THREE.Vector3(
        bbox.min.x,
        bbox.min.y + (bbox.max.y - bbox.min.y) * 0.5,
        bbox.min.z + (bbox.max.z - bbox.min.z) * 0.5
      );
    case 1:
      return new THREE.Vector3(
        bbox.max.x,
        bbox.min.y + (bbox.max.y - bbox.min.y) * 0.5,
        bbox.min.z + (bbox.max.z - bbox.min.z) * 0.5
      );
    case 2:
      return new THREE.Vector3(
        bbox.min.x + (bbox.max.x - bbox.min.x) * 0.5,
        bbox.min.y,
        bbox.min.z + (bbox.max.z - bbox.min.z) * 0.5
      );
    case 3:
      return new THREE.Vector3(
        bbox.min.x + (bbox.max.x - bbox.min.x) * 0.5,
        bbox.max.y,
        bbox.min.z + (bbox.max.z - bbox.min.z) * 0.5
      );
    case 4:
      return new THREE.Vector3(
        bbox.min.x + (bbox.max.x - bbox.min.x) * 0.5,
        bbox.min.y + (bbox.max.y - bbox.min.y) * 0.5,
        bbox.max.z
      );
    case 5:
      return new THREE.Vector3(
        bbox.min.x + (bbox.max.x - bbox.min.x) * 0.5,
        bbox.min.y + (bbox.max.y - bbox.min.y) * 0.5,
        bbox.min.z
      );
  }
};

/**
 * Check if this object contain at least one 3D mesh
 */

GZ3D.AutoAlignModel.prototype.hasMeshSubObjects = function(object) {
  var meshFound = false;

  object.traverse(function(subobj) {
    if (subobj instanceof THREE.Mesh) {
      meshFound = true;
    }
  });

  return meshFound;
};

/**
 * Align to objects
 */

GZ3D.AutoAlignModel.prototype.alignToObjects = function(position) {
  var minimalSnapDistance = GZ3D.AutoAlignModel.HorizontalObjectSnapDist;
  if (this.obj && !this.needsAlignOnFloor) {
    var bbox = new THREE.Box3();

    bbox.setFromObject(this.obj);

    if (Math.abs(bbox.min.x) !== Infinity) {
      var innerFaceCubes = [
        this.snapToObjectGenInnerFaceCube(bbox, 1, minimalSnapDistance),
        this.snapToObjectGenInnerFaceCube(bbox, 0, minimalSnapDistance),
        this.snapToObjectGenInnerFaceCube(bbox, 3, minimalSnapDistance),
        this.snapToObjectGenInnerFaceCube(bbox, 2, minimalSnapDistance)
      ];

      var that = this;
      var snapFound = false;
      var alignXFound = false;
      var alignYFound = false;
      var alignZFound = false;

      // Align faces

      this.scene.scene.traverse(function(snap2obj) {
        if (
          snap2obj.visible &&
          snap2obj instanceof THREE.Mesh &&
          !that.autoAlignIgnoreObject(snap2obj)
        ) {
          var snap2bbox = new THREE.Box3();

          snap2bbox.setFromObject(snap2obj);
          if (Math.abs(snap2bbox.min.x) !== Infinity) {
            if (!snapFound) {
              for (var j = 0; j < 4; j++) {
                var fc = that.snapToObjectGenInnerFaceCube(
                  snap2bbox,
                  j,
                  minimalSnapDistance
                );

                if (fc.intersectsBox(innerFaceCubes[j])) {
                  snapFound = true;

                  switch (j) {
                    case 0:
                      position.x =
                        snap2bbox.min.x - (bbox.max.x - that.obj.position.x);
                      that.displayAlignAxes[0].position.copy(
                        new THREE.Vector3(
                          snap2bbox.min.x,
                          position.y,
                          position.z
                        )
                      );
                      that.displayAlignAxes[0].visible = true;
                      break;

                    case 1:
                      position.x =
                        snap2bbox.max.x - (bbox.min.x - that.obj.position.x);
                      that.displayAlignAxes[1].position.copy(
                        new THREE.Vector3(
                          snap2bbox.max.x,
                          position.y,
                          position.z
                        )
                      );
                      that.displayAlignAxes[1].visible = true;
                      break;

                    case 2:
                      position.y =
                        snap2bbox.min.y - (bbox.max.y - that.obj.position.y);
                      that.displayAlignAxes[2].position.copy(
                        new THREE.Vector3(
                          position.x,
                          snap2bbox.min.y,
                          position.z
                        )
                      );
                      that.displayAlignAxes[2].visible = true;
                      break;

                    case 3:
                      position.y =
                        snap2bbox.max.y - (bbox.min.y - that.obj.position.y);
                      that.displayAlignAxes[3].position.copy(
                        new THREE.Vector3(
                          position.x,
                          snap2bbox.max.y,
                          position.z
                        )
                      );
                      that.displayAlignAxes[3].visible = true;
                      break;
                  }

                  that.obj.position.copy(position);
                  bbox.setFromObject(that.obj);
                }
              }
            }
          }
        }
      });

      // Align borders ( try first with object of same size (x/y))

      for (var i = 0; i < 2; i++) {
        if (alignXFound && alignYFound && alignZFound) {
          break;
        }

        for (var j = 0; j < this.scene.scene.children.length; j++) {
          var snap2obj = this.scene.scene.children[j];

          if (
            snap2obj.visible &&
            !that.autoAlignIgnoreObject(snap2obj) &&
            that.hasMeshSubObjects(snap2obj)
          ) {
            var snap2bbox = new THREE.Box3();

            snap2bbox.setFromObject(snap2obj);
            if (Math.abs(snap2bbox.min.x) !== Infinity) {
              if (i === 0) {
                var s = new THREE.Vector3();
                bbox.getSize(s);
                var s2 = new THREE.Vector3();
                snap2bbox.getSize(s2);
                if (
                  Math.abs(s.x - s2.x) > 0.01 ||
                  Math.abs(s.y - s2.y) > 0.01
                ) {
                  continue;
                }
              }

              if (
                !(
                  bbox.min.z >=
                    snap2bbox.max.z + GZ3D.AutoAlignModel.InnerBorder ||
                  bbox.max.z <=
                    snap2bbox.min.z + GZ3D.AutoAlignModel.InnerBorder
                )
              ) {
                if (!alignZFound) {
                  if (
                    Math.abs(snap2bbox.min.x - bbox.max.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.x - bbox.min.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.min.x - bbox.min.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.x - bbox.max.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.min.y - bbox.max.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.y - bbox.min.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.min.y - bbox.min.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.y - bbox.max.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                  ) {
                    if (
                      Math.abs(snap2bbox.min.z - bbox.min.z) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                    ) {
                      position.z =
                        snap2bbox.min.z - (bbox.min.z - that.obj.position.z);
                      alignZFound = true;
                    } else if (
                      Math.abs(snap2bbox.max.z - bbox.max.z) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                    ) {
                      position.z =
                        snap2bbox.max.z - (bbox.max.z - that.obj.position.z);
                      alignZFound = true;
                    }

                    if (alignZFound) {
                      that.obj.position.copy(position);
                      bbox.setFromObject(that.obj);
                    }
                  }
                }

                if (!alignYFound) {
                  if (
                    Math.abs(snap2bbox.min.x - bbox.max.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.x - bbox.min.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.min.x - bbox.min.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.x - bbox.max.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.min.z - bbox.max.z) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.z - bbox.min.z) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                  ) {
                    if (
                      Math.abs(snap2bbox.min.y - bbox.min.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                    ) {
                      position.y =
                        snap2bbox.min.y - (bbox.min.y - that.obj.position.y);
                      this.displayAlignAxes[2].position.copy(
                        new THREE.Vector3(
                          position.x,
                          snap2bbox.min.y,
                          position.z
                        )
                      );
                      this.displayAlignAxes[2].visible = true;
                      alignYFound = true;
                    }
                    if (
                      Math.abs(snap2bbox.max.y - bbox.max.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                    ) {
                      position.y =
                        snap2bbox.max.y - (bbox.max.y - that.obj.position.y);
                      this.displayAlignAxes[3].position.copy(
                        new THREE.Vector3(
                          position.x,
                          snap2bbox.max.y,
                          position.z
                        )
                      );
                      this.displayAlignAxes[3].visible = true;
                      alignYFound = true;
                    }

                    if (alignYFound) {
                      that.obj.position.copy(position);
                      bbox.setFromObject(that.obj);
                    }
                  }
                }

                if (!alignXFound) {
                  if (
                    Math.abs(snap2bbox.min.y - bbox.max.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.y - bbox.min.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.min.y - bbox.min.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.y - bbox.max.y) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.min.z - bbox.max.z) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist ||
                    Math.abs(snap2bbox.max.z - bbox.min.z) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                  ) {
                    if (
                      Math.abs(snap2bbox.min.x - bbox.min.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                    ) {
                      position.x =
                        snap2bbox.min.x - (bbox.min.x - that.obj.position.x);
                      this.displayAlignAxes[0].position.copy(
                        new THREE.Vector3(
                          snap2bbox.min.x,
                          position.y,
                          position.z
                        )
                      );
                      this.displayAlignAxes[0].visible = true;
                      alignXFound = true;
                    }
                    if (
                      Math.abs(snap2bbox.max.x - bbox.max.x) <
                      GZ3D.AutoAlignModel.AutoAlignSnapDist
                    ) {
                      position.x =
                        snap2bbox.max.x - (bbox.max.x - that.obj.position.x);
                      this.displayAlignAxes[1].position.copy(
                        new THREE.Vector3(
                          snap2bbox.max.x,
                          position.y,
                          position.z
                        )
                      );
                      this.displayAlignAxes[1].visible = true;
                      alignXFound = true;
                    }

                    if (alignXFound) {
                      that.obj.position.copy(position);
                      bbox.setFromObject(that.obj);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  return position;
};

/**
 * Align snap object on target hitten mesh
 *
 */

GZ3D.AutoAlignModel.prototype.alignOnShape = function(position, hitDesc) {
  var bbox = new THREE.Box3().setFromObject(this.obj);
  if (Math.abs(bbox.min.x) !== Infinity) {
    var alignOnGround = true;

    if (hitDesc && hitDesc.object && hitDesc.face) {
      var av, bv, cv;

      if (hitDesc.object.geometry.vertices) {
        av = hitDesc.object.geometry.vertices[hitDesc.face.a];
        bv = hitDesc.object.geometry.vertices[hitDesc.face.b];
        cv = hitDesc.object.geometry.vertices[hitDesc.face.c];
      } else if (hitDesc.object.geometry.attributes) {
        var itemSize = hitDesc.object.geometry.attributes.position.itemSize;
        var array = hitDesc.object.geometry.attributes.position.array;
        var i;

        i = hitDesc.face.a * itemSize;
        av = new THREE.Vector3(array[i], array[i + 1], array[i + 2]);

        i = hitDesc.face.b * itemSize;
        bv = new THREE.Vector3(array[i], array[i + 1], array[i + 2]);

        i = hitDesc.face.c * itemSize;
        cv = new THREE.Vector3(array[i], array[i + 1], array[i + 2]);
      } else {
        return;
      }

      var q = new THREE.Quaternion();
      hitDesc.object.getWorldQuaternion(q);

      var abv = new THREE.Vector3();
      var acv = new THREE.Vector3();

      abv.subVectors(bv, av);
      acv.subVectors(cv, av);

      var v = new THREE.Vector3();
      v.copy(abv);

      v.cross(acv);
      v.normalize();

      v.applyQuaternion(q);

      if (Math.abs(v.x) > Math.abs(v.y)) {
        v.y = 0;

        if (Math.abs(v.x) > Math.abs(v.z)) {
          v.z = 0;
        } else {
          v.x = 0;
        }
      } else {
        v.x = 0;

        if (Math.abs(v.y) > Math.abs(v.z)) {
          v.z = 0;
        } else {
          v.y = 0;
        }
      }

      v.normalize();

      if (v.x > 0) {
        position.x = position.x - (bbox.min.x - this.obj.position.x);
      } else if (v.x < 0) {
        position.x = position.x - (bbox.max.x - this.obj.position.x);
      } else if (v.y > 0) {
        position.y = position.y - (bbox.min.y - this.obj.position.y);
      } else if (v.y < 0) {
        position.y = position.y - (bbox.max.y - this.obj.position.y);
      } else if (v.z < 0) {
        position.z = position.z - (bbox.max.z - this.obj.position.z);
        alignOnGround = false;
      }
    } else {
      alignOnGround = false;
      var resInfo = this.findBestSurfaceToAlignZ(position.z);
      if (resInfo) {
        position.z = resInfo.lowerZ;
        var centerbbox = new THREE.Box3().setFromObject(resInfo.lowerObject);
        if (Math.abs(centerbbox.min.x) !== Infinity) {
          var pt1 = this.gravityPointOfFace(centerbbox, 4);
          var pt2 = this.gravityPointOfFace(bbox, 5);
          var vx = pt2.x - pt1.x;
          var vy = pt2.y - pt1.y;
          var minDist = GZ3D.AutoAlignModel.AutoAlignSnapDist;

          if (vx * vx + vy * vy < minDist * minDist) {
            position.x =
              pt1.x -
              (bbox.max.x - this.obj.position.x) +
              (bbox.max.x - bbox.min.x) * 0.5;
            position.y =
              pt1.y -
              (bbox.max.y - this.obj.position.y) +
              (bbox.max.y - bbox.min.y) * 0.5;
          }
        }
      }
    }

    if (alignOnGround) {
      position.z = position.z - (bbox.min.z - this.obj.position.z);
    }
  }

  return position;
};

/**
 * Find the best Z value/object for auto-align object
 *
 */

GZ3D.AutoAlignModel.prototype.findBestSurfaceToAlignZ = function(fromZ) {
  var bbox = new THREE.Box3().setFromObject(this.obj);
  if (Math.abs(bbox.min.x) !== Infinity) {
    var point = this.obj.position.clone();
    var gpoint = point.clone();
    var lowerZ;
    var raycastResult;

    point.z = fromZ;
    gpoint.z = fromZ + (bbox.max.z - bbox.min.z) * 0.5;

    raycastResult = this.raycastToLowerZ(gpoint, new THREE.Vector3(0, 0, 1));
    if (raycastResult !== undefined) {
      point.z = raycastResult.lowerZ - 0.01;
    } else {
      point.z += 50.0;
    }

    raycastResult = this.raycastToLowerZ(point, new THREE.Vector3(0, 0, -1));
    if (raycastResult !== undefined) {
      raycastResult.lowerZ =
        raycastResult.lowerZ - (bbox.min.z - this.obj.position.z);
      return raycastResult;
    }
  }

  return undefined;
};

/**
 * Find the best possible Z value so the mesh is not under the ground, when we add it.
 *
 */

GZ3D.AutoAlignModel.prototype.findBestZ = function(fromZ) {
  if (this.obj && !this.needsAlignOnFloor) {
    var resInfo = this.findBestSurfaceToAlignZ(fromZ);
    if (resInfo) {
      return resInfo.lowerZ;
    }

    return this.obj.position.z;
  }

  return 0;
};

/**
 * Start aligning model.
 */

GZ3D.AutoAlignModel.prototype.start = function(objectToAlign) {
  this.obj = objectToAlign;
  this.disableSnap = false;
  this.initUIHelpers();
};

/**
 * Finish aligning
 */

GZ3D.AutoAlignModel.prototype.finish = function() {
  var that = this;

  this.obj = undefined;
  this.needsAlignOnFloor = false;
  this.hideUIHelpers();
};

/**
 * Move and align model
 *
 */

GZ3D.AutoAlignModel.prototype.moveAlignModel = function(positionX, positionY) {
  var raycaster = new THREE.Raycaster();
  var point, i;
  var distance = -1000;
  var hitDesc;

  var viewWidth = this.scene.getDomElement().clientWidth;
  var viewHeight = this.scene.getDomElement().clientHeight;
  var originalObjPos = new THREE.Vector3(
    this.obj.position.x,
    this.obj.position.y,
    this.obj.position.z
  );

  this.hideUIHelpers();

  if (!this.disableSnap) {
    raycaster.setFromCamera(
      new THREE.Vector2(
        positionX / viewWidth * 2 - 1,
        -(positionY / viewHeight) * 2 + 1
      ),
      this.scene.camera
    );

    var objects = raycaster.intersectObjects(this.scene.scene.children, true);

    for (i = 0; i < objects.length; i++) {
      var idesc = objects[i];

      if (!this.autoAlignIgnoreObject(idesc.object)) {
        if (idesc.distance < distance || !point) {
          distance = idesc.distance;
          point = idesc.point.clone();
          hitDesc = idesc;
        }
      }
    }
  }

  if (!point) {
    var vector = new THREE.Vector3(
      positionX / viewWidth * 2 - 1,
      -(positionY / viewHeight) * 2 + 1,
      0.5
    );

    vector.unproject(this.scene.camera);

    var camv = this.scene.camera.position.clone();
    camv.z -= this.obj.position.z;

    this.ray.set(camv, vector.sub(this.scene.camera.position).normalize());
    point = this.ray.intersectPlane(this.plane);
  }

  if (point) {
    if (!this.disableSnap) {
      var snapDist = this.scene.modelManipulator.snapDist;
      if (snapDist) {
        point.x = Math.round(point.x / snapDist) * snapDist;
        point.y = Math.round(point.y / snapDist) * snapDist;
        point.z = Math.round(point.z / snapDist) * snapDist;
      }
    }

    if (!this.disableSnap) {
      this.obj.position.copy(point);
      point = this.alignOnShape(point, hitDesc);
    } else {
      point.z = this.obj.position.z;
    }

    if (this.scene.modelManipulator.lockZAxis) {
      point.z = originalObjPos.z;
    }

    if (!this.disableSnap) {
      this.obj.position.copy(point);
      point = this.alignToObjects(point);
    }

    if (this.scene.modelManipulator.lockXAxis) {
      point.x = originalObjPos.x;
    }

    if (this.scene.modelManipulator.lockYAxis) {
      point.y = originalObjPos.y;
    }

    if (this.scene.modelManipulator.lockZAxis) {
      point.z = originalObjPos.z;
    }

    this.scene.setPose(this.obj, point, this.obj.quaternion);
  }
};

/**
 * Rotate 90 deg.
 *
 */

GZ3D.AutoAlignModel.prototype.quickRotate = function() {
  if (this.obj) {
    var q = this.obj.quaternion.clone();
    var qr = new THREE.Quaternion();

    qr.setFromAxisAngle(new THREE.Vector3(0, 0, 1), Math.PI / 2);
    q.multiply(qr);

    this.scene.setPose(this.obj, this.obj.position, q);
  }
};

/**
 * Keyboard shortcuts
 *
 */

GZ3D.AutoAlignModel.prototype.onKeyUp = function(event) {
  if (event.key === 'Shift') {
    this.disableSnap = false;
  }
};

GZ3D.AutoAlignModel.prototype.onKeyDown = function(event) {
  if (event.keyCode === 32) {
    // Quick rotation
    this.quickRotate();
  }

  if (event.key === 'Shift') {
    this.disableSnap = true;
  }
};
