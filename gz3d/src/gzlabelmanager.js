/**
 * HBP
 *
 * Manage labels in the 3D scene.
 *
 */

GZ3D.LabelManager = function(gz3dScene) {
  this.scene = gz3dScene;

  this.init();
};

GZ3D.LabelManager.prototype.init = function() {
  this.sensorIcons = {};
  this.perObjectLabels = {};

  this.initMeshIcons();
};

GZ3D.LabelManager.prototype.initMeshIcons = function() {
  var loader = new THREE.ColladaLoader();

  var textureLoader = new THREE.TextureLoader();

  this.sensorIcons['generic'] = textureLoader.load(
    'img/3denv/icons/sensor_Generic.png'
  );
  this.sensorIcons['ray'] = textureLoader.load(
    'img/3denv/icons/sensor_Ray.png'
  );
  this.sensorIcons['camera'] = textureLoader.load(
    'img/3denv/icons/sensor_Camera.png'
  );

  var that = this;
  loader.load('img/3denv/icons/sensor.dae', function(collada) {
    var dae = collada.scene;
    dae.updateMatrix();

    that.sensorMesh = dae;
  });

  loader.load('img/3denv/icons/joint.dae', function(collada) {
    var dae = collada.scene;
    dae.updateMatrix();

    that.jointMesh = dae;
  });
};

GZ3D.LabelManager.prototype.createJoint3DIcon = function(jointObj, joint) {
  var jointIcon = 'generic';
  var jointIconMesh = this.jointMesh.clone();
  var material = new THREE.MeshBasicMaterial();
  material.map = this.sensorIcons[jointIcon];
  jointIconMesh.children[0].material = material;
  jointIconMesh.name = 'LABEL_INFO_VISUAL';
  jointIconMesh.castShadow = false;
  jointIconMesh.receiveShadow = false;
  jointIconMesh.visible = true;
  jointIconMesh.scale.set(0.8, 0.8, 0.8);
  jointObj.add(jointIconMesh);
  return jointIconMesh;
};

GZ3D.LabelManager.prototype.createSensor3DIcon = function(sensorObj, sensor) {
  var sensorIcon = sensor.type in this.sensorIcons ? sensor.type : 'generic';
  var sensorIconMesh = this.sensorMesh.clone();
  var material = new THREE.MeshBasicMaterial();
  material.map = this.sensorIcons[sensorIcon];
  sensorIconMesh.children[0].material = material;
  sensorIconMesh.name = 'LABEL_INFO_VISUAL';
  sensorIconMesh.castShadow = false;
  sensorIconMesh.receiveShadow = false;
  sensorIconMesh.visible = true;
  sensorIconMesh.scale.set(0.8, 0.8, 0.8);
  sensorObj.add(sensorIconMesh);
  return sensorIconMesh;
};

GZ3D.LabelManager.prototype.createLabel = function(text, link, parent) {
  // function for drawing rounded rectangles
  var roundRect = function(ctx, x, y, w, h, r) {
    ctx.beginPath();
    ctx.moveTo(x + r, y);
    ctx.lineTo(x + w - r, y);
    ctx.quadraticCurveTo(x + w, y, x + w, y + r);
    ctx.lineTo(x + w, y + h - r);
    ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
    ctx.lineTo(x + r, y + h);
    ctx.quadraticCurveTo(x, y + h, x, y + h - r);
    ctx.lineTo(x, y + r);
    ctx.quadraticCurveTo(x, y, x + r, y);
    ctx.closePath();
    ctx.fill();
    context.lineWidth = 5;
    ctx.stroke();
  };

  var canvas = document.createElement('canvas');
  var size = 0.4;
  canvas.width = 512;
  canvas.height = 64;
  var context = canvas.getContext('2d');
  context.textAlign = 'center';

  context.font = '32px Arial';

  var boxSize = context.measureText(text).width + 30;
  var boxHeight = 45;
  if (boxSize > canvas.width) {
    boxSize = canvas.width;
  }

  if (link) {
    context.fillStyle = 'black';
    context.strokeStyle = 'gray';
  } else {
    context.fillStyle = 'white';
    context.strokeStyle = 'gray';
  }
  roundRect(context, canvas.width / 2 - boxSize / 2, 2, boxSize, boxHeight, 15);

  roundRect(context, canvas.width / 2 - boxSize / 2, 2, boxSize, boxHeight, 15);

  if (link) {
    context.fillStyle = 'white';
  } else {
    context.fillStyle = 'black';
  }

  context.fillText(text, canvas.width / 2, canvas.height / 2);
  var texture = new THREE.Texture(canvas);
  texture.needsUpdate = true;

  var geometry = new THREE.PlaneGeometry(
    size,
    canvas.height / canvas.width * size
  );
  var material = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    side: THREE.DoubleSide,
    map: texture
  });
  material.transparent = true;
  material.depthTest = false;
  material.depthWrite = false;

  var plane = new THREE.Mesh(geometry, material);
  plane._ignoreRaycast = true;
  plane.visible = true;
  plane.name = 'LABEL_INFO_VISUAL';

  var hitGeometry = new THREE.PlaneGeometry(
    size / (canvas.width / boxSize),
    size / (canvas.height / boxHeight / (canvas.height / canvas.width))
  );
  var hitPlane = new THREE.Mesh(hitGeometry, material);
  hitPlane.visible = false;
  hitPlane._raycastOnly = true;
  hitPlane.name = 'LABEL_INFO_VISUAL';

  if (!parent._relatedLabels) {
    parent._relatedLabels = [];
  }

  parent._relatedLabels.push(hitPlane);
  parent._relatedLabels.push(plane);

  this.scene.scene.add(hitPlane);
  this.scene.scene.add(plane);

  hitPlane._labelOwner = parent;
  plane._labelOwner = parent;

  plane._hitPlane = hitPlane;

  return plane;
};

GZ3D.LabelManager.prototype.onRender = function() {
  var tempVector = new THREE.Vector3();
  var bbox = new THREE.Box3();
  var nodePos = new THREE.Vector3(),
    q = new THREE.Quaternion(),
    s = new THREE.Vector3();

  for (var key in this.perObjectLabels) {
    if (this.perObjectLabels.hasOwnProperty(key)) {
      var labelInfoList = this.perObjectLabels[key];

      if (labelInfoList.length && labelInfoList[0].root._labelsVisible) {
        var root = labelInfoList[0].root;
        var labelPos = [];

        bbox.setFromObject(root);
        var bsphere = new THREE.Sphere();
        bbox.getBoundingSphere(bsphere);

        for (var i = 0; i < labelInfoList.length; i++) {
          var labelInfo = labelInfoList[i];
          var node = labelInfo.node;
          var line = labelInfo.line;

          // Label

          node.matrixWorld.decompose(nodePos, q, s);

          var dir = nodePos.clone();
          dir.sub(bsphere.center);

          var l = dir.length();

          if (l <= Number.EPSILON || isNaN(l)) {
            dir.set(0, 0, -1);
          } else {
            dir.divideScalar(l);
          }

          dir.multiplyScalar(bsphere.radius);

          var finalLabelPos = bsphere.center.clone();
          finalLabelPos.add(dir);

          finalLabelPos.z += 0.25;

          if (finalLabelPos.z < bbox.min.z) {
            finalLabelPos.z = bbox.min.z + 0.1;

            dir.set(finalLabelPos.x, finalLabelPos.y, 0);
            dir.normalize();
            dir.multiplyScalar(bsphere.radius + 0.1);

            finalLabelPos = bsphere.center.clone();
            finalLabelPos.add(dir);
          }

          // Check if it is too close from another label
          // We keep it simple, because it has to be in real-time

          dir.normalize();
          dir.multiplyScalar(0.08);

          for (var li = 0; li < labelPos.length; li++) {
            tempVector.copy(finalLabelPos);
            tempVector.sub(labelPos[li]);

            if (tempVector.length() < 0.1) {
              finalLabelPos.add(dir);
            }
          }

          labelPos.push(finalLabelPos);
          labelInfo.label.position.copy(finalLabelPos);
          labelInfo.label.quaternion.copy(this.scene.camera.quaternion);

          if (labelInfo.label._hitPlane) {
            labelInfo.label._hitPlane.position.copy(finalLabelPos);
            labelInfo.label._hitPlane.quaternion.copy(
              this.scene.camera.quaternion
            );
          }

          // Line

          line.position.copy(nodePos);

          if (!line.geometry.vertices[1].equals(nodePos)) {
            line.geometry.vertices[1] = labelInfo.label.position
              .clone()
              .sub(nodePos);
            line.geometry.verticesNeedUpdate = true;
          }
        }
      }
    }
  }
};

GZ3D.LabelManager.prototype.createLineLabel = function(parent) {
  var lineGeometry = new THREE.Geometry();
  var vertArray = lineGeometry.vertices;
  vertArray.push(new THREE.Vector3(0, 0, 0), new THREE.Vector3(1, 1, 1));

  var lineMaterial = new THREE.LineBasicMaterial({
    color: 0x888888,
    linewidth: 3
  });
  var line = new THREE.Line(lineGeometry, lineMaterial);
  line.computeLineDistances();

  this.scene.scene.add(line);

  line.name = 'LABEL_INFO_VISUAL';
  line._ignoreRaycast = true;

  if (!parent._relatedLabels) {
    parent._relatedLabels = [];
  }

  parent._relatedLabels.push(line);

  return line;
};

GZ3D.LabelManager.prototype.setLabelInfoVisible = function(root, visible) {
  var that = this;
  var iconMesh;
  var labelMesh;
  var labelLine;

  root._labelsVisible = visible;

  root.traverse(function(node) {
    if (node._linkSource) {
      if (!node._labelInitialized && visible) {
        node._labelInitialized = true;
        iconMesh = that.createJoint3DIcon(node, node._linkSource);
        labelLine = that.createLineLabel(node);
        labelMesh = that.createLabel(node._linkSource.name, true, node);

        if (!that.perObjectLabels[root.uuid]) {
          that.perObjectLabels[root.uuid] = [];
        }

        that.perObjectLabels[root.uuid].push({
          root: root,
          node: node,
          icon3d: iconMesh,
          label: labelMesh,
          line: labelLine
        });
      }
    } else if (node._sensorSource) {
      if (!node._labelInitialized && visible) {
        node._labelInitialized = true;
        iconMesh = that.createSensor3DIcon(node, node._sensorSource);
        labelLine = that.createLineLabel(node);
        labelMesh = that.createLabel(node._sensorSource.name, false, node);

        if (!that.perObjectLabels[root.uuid]) {
          that.perObjectLabels[root.uuid] = [];
        }

        that.perObjectLabels[root.uuid].push({
          root: root,
          node: node,
          icon3d: iconMesh,
          label: labelMesh,
          line: labelLine
        });
      }
    } else if (node.name === 'LABEL_INFO_VISUAL' && !node._raycastOnly) {
      node.visible = visible;
    }

    if (node._relatedLabels) {
      for (var i = 0; i < node._relatedLabels.length; i++) {
        if (!node._relatedLabels[i]._raycastOnly) {
          node._relatedLabels[i].visible = visible;
        }
      }
    }
  });
};
