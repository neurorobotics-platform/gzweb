// Based on TransformControls.js
// original author: arodic / https://github.com/arodic

GZ3D.TRANSFORM_TYPE_NAME_PREFIX = {
  TRANSLATE: 'T',
  SCALE: 'S',
  ROTATE: 'R',
  ALL: 'A',
  NONE: 'N'
};

/**
 * Manipulator to perform translate, scale and rotate transforms on objects
 * within the scene.
 * @constructor
 */
GZ3D.Manipulator = function(gz3dScene, mobile) {
  this.gz3dScene = gz3dScene;
  this.userView = this.gz3dScene.viewManager.mainUserView;

  // Mobile / desktop
  this.mobile = mobile !== undefined ? mobile : false;

  // Object to be manipulated
  this.object = undefined;

  // translate / rotate / scale
  this.mode = 'translate';

  // world / local
  this.space = 'world';

  // axis lock

  this.lockXAxis = false;
  this.lockYAxis = false;
  this.lockZAxis = false;

  // hovered used for backwards compatibility
  // Whenever it wasn't an issue, hovered and active were combined
  // into selected
  this.hovered = false;
  this.selected = 'null';

  this.scale = 1;

  this.snapDist = null;
  this.modifierAxis = new THREE.Vector3(1, 1, 1);
  this.gizmo = new THREE.Object3D();

  this.pickerNames = [];
  this.pickerMeshes = [];

  var scope = this;

  var changeEvent = function(_transform) {
    //TRANSFORM_TYPE_NAME_PREFIX

    if (_transform) {
      return { type: 'change', transform: _transform };
    } else {
      return {
        type: 'change',
        transform: GZ3D.TRANSFORM_TYPE_NAME_PREFIX.NONE
      };
    }
  };

  var ray = new THREE.Raycaster();
  var pointerVector = new THREE.Vector3();
  var startRay = new THREE.Ray();
  var lastRay = new THREE.Ray();

  var point = new THREE.Vector3();
  var offset = new THREE.Vector3();

  var rotation = new THREE.Vector3();
  var offsetRotation = new THREE.Vector3();
  var scale = 1;

  var lookAtMatrix = new THREE.Matrix4();
  var eye = new THREE.Vector3();

  var tempMatrix = new THREE.Matrix4();
  var tempVector = new THREE.Vector3();
  var tempQuaternion = new THREE.Quaternion();
  var unitX = new THREE.Vector3(1, 0, 0);
  var unitY = new THREE.Vector3(0, 1, 0);
  var unitZ = new THREE.Vector3(0, 0, 1);

  var quaternionXYZ = new THREE.Quaternion();
  var quaternionX = new THREE.Quaternion();
  var quaternionY = new THREE.Quaternion();
  var quaternionZ = new THREE.Quaternion();
  var quaternionE = new THREE.Quaternion();

  var oldPosition = new THREE.Vector3();
  var oldRotationMatrix = new THREE.Matrix4();
  var oldScale = new THREE.Vector3();

  var parentRotationMatrix = new THREE.Matrix4();
  var parentScale = new THREE.Vector3();

  var worldPosition = new THREE.Vector3();
  var worldRotation = new THREE.Vector3();
  var worldRotationMatrix = new THREE.Matrix4();
  var camPosition = new THREE.Vector3();

  var hovered = null;
  var hoveredColor = new THREE.Color();

  // Picker currently selected (highlighted)
  var selectedPicker = null;
  var selectedColor = new THREE.Color();

  // Intersection planes
  var intersectionPlanes = {};
  var intersectionPlaneList = ['XY', 'YZ', 'XZ'];
  var currentPlane = 'XY';

  var planes = new THREE.Object3D();
  this.gizmo.add(planes);

  for (var i in intersectionPlaneList) {
    var currentIntersectionPlane = new THREE.Mesh(
      new THREE.PlaneGeometry(500, 500)
    );

    currentIntersectionPlane.material.side = THREE.DoubleSide;
    currentIntersectionPlane.visible = false;

    intersectionPlanes[intersectionPlaneList[i]] = currentIntersectionPlane;
    planes.add(currentIntersectionPlane);
  }

  intersectionPlanes['YZ'].rotation.set(0, Math.PI / 2, 0);
  intersectionPlanes['XZ'].rotation.set(-Math.PI / 2, 0, 0);
  bakeTransformations(intersectionPlanes['YZ']);
  bakeTransformations(intersectionPlanes['XZ']);

  // Geometries

  var pickerAxes = {};
  var displayAxes = {};

  var HandleMaterial = function(color, over, opacity) {
    var material = new THREE.MeshBasicMaterial();
    material.color = color;
    if (over) {
      material.side = THREE.DoubleSide;
      material.depthTest = false;
      material.depthWrite = false;
    }
    material.opacity = opacity !== undefined ? opacity : 0.5;
    material.transparent = true;
    return material;
  };

  var LineMaterial = function(color, opacity) {
    var material = new THREE.LineBasicMaterial();
    material.color = color;
    material.depthTest = false;
    material.depthWrite = false;
    material.opacity = opacity !== undefined ? opacity : 1;
    material.transparent = true;
    material.linewidth = 2;
    return material;
  };

  // Colors
  var white = new THREE.Color(0xffffff);
  var red = new THREE.Color(0xff0000);
  var green = new THREE.Color(0x00ff00);
  var blue = new THREE.Color(0x0000ff);
  var cyan = new THREE.Color(0x00ffff);
  var magenta = new THREE.Color(0xff00ff);
  var yellow = new THREE.Color(0xffff00);

  function buildArrowManipulator(transformType) {
    // 'translate' or 'scale'

    var geometry, mesh;

    var transformTypePrefix =
      transformType === 'translate'
        ? GZ3D.TRANSFORM_TYPE_NAME_PREFIX.TRANSLATE
        : GZ3D.TRANSFORM_TYPE_NAME_PREFIX.SCALE;

    pickerAxes[transformType] = new THREE.Object3D();
    displayAxes[transformType] = new THREE.Object3D();
    scope.gizmo.add(pickerAxes[transformType]);
    scope.gizmo.add(displayAxes[transformType]);

    // Picker cylinder
    if (scope.mobile) {
      geometry = new THREE.CylinderGeometry(0.5, 0.01, 1.4, 10, 1, false);
    } else {
      geometry = new THREE.CylinderGeometry(0.2, 0.1, 0.8, 4, 1, false);
    }

    mesh = new THREE.Mesh(geometry, new HandleMaterial(red, true));
    mesh.position.x = 0.7;
    mesh.rotation.z = -Math.PI / 2;
    bakeTransformations(mesh);
    mesh.name = transformTypePrefix + 'X';
    pickerAxes[transformType].add(mesh);
    scope.pickerNames.push(mesh.name);
    scope.pickerMeshes[mesh.name] = mesh;

    mesh = new THREE.Mesh(geometry, new HandleMaterial(green, true));
    mesh.position.y = 0.7;
    bakeTransformations(mesh);
    mesh.name = transformTypePrefix + 'Y';
    pickerAxes[transformType].add(mesh);
    scope.pickerNames.push(mesh.name);
    scope.pickerMeshes[mesh.name] = mesh;

    mesh = new THREE.Mesh(geometry, new HandleMaterial(blue, true));
    mesh.position.z = 0.7;
    mesh.rotation.x = Math.PI / 2;
    bakeTransformations(mesh);
    mesh.name = transformTypePrefix + 'Z';
    pickerAxes[transformType].add(mesh);
    scope.pickerNames.push(mesh.name);
    scope.pickerMeshes[mesh.name] = mesh;

    if (scope.mobile) {
      // Display cylinder
      geometry = new THREE.CylinderGeometry(0.1, 0.1, 1, 10, 1, false);

      mesh = new THREE.Mesh(geometry, new HandleMaterial(red, true));
      mesh.position.x = 0.5;
      mesh.rotation.z = -Math.PI / 2;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'X';
      displayAxes[transformType].add(mesh);

      mesh = new THREE.Mesh(geometry, new HandleMaterial(green, true));
      mesh.position.y = 0.5;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'Y';
      displayAxes[transformType].add(mesh);

      mesh = new THREE.Mesh(geometry, new HandleMaterial(blue, true));
      mesh.position.z = 0.5;
      mesh.rotation.x = Math.PI / 2;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'Z';
      displayAxes[transformType].add(mesh);

      // Display arrow tip
      if (transformTypePrefix === GZ3D.TRANSFORM_TYPE_NAME_PREFIX.TRANSLATE) {
        // translate -> cone tip
        geometry = new THREE.CylinderGeometry(0, 0.15, 0.4, 10, 1, false);
      } else {
        // scale -> box tip
        geometry = new THREE.BoxGeometry(0.3, 0.5, 0.3);
      }

      mesh = new THREE.Mesh(geometry, new HandleMaterial(red, true));
      mesh.position.x = 1.2;
      mesh.rotation.z = -Math.PI / 2;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'X';
      displayAxes[transformType].add(mesh);

      mesh = new THREE.Mesh(geometry, new HandleMaterial(green, true));
      mesh.position.y = 1.2;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'Y';
      displayAxes[transformType].add(mesh);

      mesh = new THREE.Mesh(geometry, new HandleMaterial(blue, true));
      mesh.position.z = 1.2;
      mesh.rotation.x = Math.PI / 2;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'Z';
      displayAxes[transformType].add(mesh);
    } else {
      // Display lines
      geometry = new THREE.Geometry();
      geometry.vertices.push(
        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(1, 0, 0),
        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(0, 1, 0),
        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(0, 0, 1)
      );
      geometry.colors.push(red, red, green, green, blue, blue);
      var material = new THREE.LineBasicMaterial({
        vertexColors: THREE.VertexColors,
        depthTest: false,
        depthWrite: false,
        transparent: true,
        linewidth: 2
      });
      mesh = new THREE.LineSegments(geometry, material);
      displayAxes[transformType].add(mesh);

      // Display (arrow tip)
      if (transformType === 'translate') {
        // translate -> cone tip
        geometry = new THREE.CylinderGeometry(0, 0.05, 0.2, 4, 1, true);
      } else {
        // scale -> box tip
        geometry = new THREE.BoxGeometry(0.1, 0.2, 0.1);
      }

      mesh = new THREE.Mesh(geometry, new HandleMaterial(red, true, 1));
      mesh.position.x = 1.1;
      mesh.rotation.z = -Math.PI / 2;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'X';
      displayAxes[transformType].add(mesh);

      mesh = new THREE.Mesh(geometry, new HandleMaterial(green, true, 1));
      mesh.position.y = 1.1;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'Y';
      displayAxes[transformType].add(mesh);

      mesh = new THREE.Mesh(geometry, new HandleMaterial(blue, true, 1));
      mesh.position.z = 1.1;
      mesh.rotation.x = Math.PI / 2;
      bakeTransformations(mesh);
      mesh.name = transformTypePrefix + 'Z';
      displayAxes[transformType].add(mesh);

      if (transformType === 'translate') {
        // Picker and display octahedron for TXYZ
        var T = transformTypePrefix;
        mesh = new THREE.Mesh(
          new THREE.OctahedronGeometry(0.1, 0),
          new HandleMaterial(white, true, 0.25)
        );
        mesh.name = T + 'XYZ';
        scope.pickerNames.push(mesh.name);
        displayAxes[transformType].add(mesh);
        pickerAxes[transformType].add(mesh.clone());

        // Picker and display planes
        geometry = new THREE.PlaneGeometry(0.3, 0.3);

        mesh = new THREE.Mesh(geometry, new HandleMaterial(yellow, 0.25));
        mesh.position.set(0.15, 0.15, 0);
        bakeTransformations(mesh);
        mesh.name = T + 'XY';
        scope.pickerNames.push(mesh.name);
        displayAxes[transformType].add(mesh);
        pickerAxes[transformType].add(mesh.clone());

        mesh = new THREE.Mesh(geometry, new HandleMaterial(cyan, 0.25));
        mesh.position.set(0, 0.15, 0.15);
        mesh.rotation.y = Math.PI / 2;
        bakeTransformations(mesh);
        mesh.name = T + 'YZ';
        scope.pickerNames.push(mesh.name);
        displayAxes[transformType].add(mesh);
        pickerAxes[transformType].add(mesh.clone());

        mesh = new THREE.Mesh(geometry, new HandleMaterial(magenta, 0.25));
        mesh.position.set(0.15, 0, 0.15);
        mesh.rotation.x = Math.PI / 2;
        bakeTransformations(mesh);
        mesh.name = T + 'XZ';
        scope.pickerNames.push(mesh.name);
        displayAxes[transformType].add(mesh);
        pickerAxes[transformType].add(mesh.clone());
      }
    }
  }

  // Translate
  buildArrowManipulator('translate');

  // Scale
  buildArrowManipulator('scale');

  // Rotate
  var geometry, mesh;

  pickerAxes['rotate'] = new THREE.Object3D();
  displayAxes['rotate'] = new THREE.Object3D();
  this.gizmo.add(pickerAxes['rotate']);
  this.gizmo.add(displayAxes['rotate']);

  // RX, RY, RZ

  // Picker torus
  if (this.mobile) {
    geometry = new THREE.TorusGeometry(1, 0.3, 4, 36, 2 * Math.PI);
  } else {
    geometry = new THREE.TorusGeometry(1, 0.15, 4, 6, Math.PI);
  }

  mesh = new THREE.Mesh(geometry, new HandleMaterial(red, false));
  mesh.rotation.z = -Math.PI / 2;
  mesh.rotation.y = -Math.PI / 2;
  bakeTransformations(mesh);
  mesh.name = 'RX';
  pickerAxes['rotate'].add(mesh);
  this.pickerNames.push(mesh.name);
  this.pickerMeshes[mesh.name] = mesh;

  mesh = new THREE.Mesh(geometry, new HandleMaterial(green, false));
  mesh.rotation.z = Math.PI;
  mesh.rotation.x = -Math.PI / 2;
  bakeTransformations(mesh);
  mesh.name = 'RY';
  pickerAxes['rotate'].add(mesh);
  this.pickerNames.push(mesh.name);
  this.pickerMeshes[mesh.name] = mesh;

  mesh = new THREE.Mesh(geometry, new HandleMaterial(blue, false));
  mesh.rotation.z = -Math.PI / 2;
  bakeTransformations(mesh);
  mesh.name = 'RZ';
  pickerAxes['rotate'].add(mesh);
  this.pickerNames.push(mesh.name);
  this.pickerMeshes[mesh.name] = mesh;

  if (this.mobile) {
    // Display torus
    geometry = new THREE.TorusGeometry(1, 0.1, 4, 36, 2 * Math.PI);

    mesh = new THREE.Mesh(geometry, new HandleMaterial(blue, false));
    mesh.rotation.z = -Math.PI / 2;
    bakeTransformations(mesh);
    mesh.name = 'RZ';
    displayAxes['rotate'].add(mesh);

    mesh = new THREE.Mesh(geometry, new HandleMaterial(red, false));
    mesh.rotation.z = -Math.PI / 2;
    mesh.rotation.y = -Math.PI / 2;
    bakeTransformations(mesh);
    mesh.name = 'RX';
    displayAxes['rotate'].add(mesh);

    mesh = new THREE.Mesh(geometry, new HandleMaterial(green, false));
    mesh.rotation.z = Math.PI;
    mesh.rotation.x = -Math.PI / 2;
    bakeTransformations(mesh);
    mesh.name = 'RY';
    displayAxes['rotate'].add(mesh);
  } else {
    // Display circles
    var Circle = function(radius, facing, start, end) {
      var geometry = new THREE.Geometry();
      start = start ? start * 2.0 * Math.PI : 0.0;
      end = end ? end * 2.0 * Math.PI : 2.0 * Math.PI;
      const numberOfSegments = 64.0;
      const angleIncrement = 2.0 * Math.PI / numberOfSegments;
      for (var angle = start; angle <= end; angle += angleIncrement) {
        var point;
        const cosinus = Math.cos(angle);
        const sinus = Math.sin(angle);
        switch (facing) {
          case 'x':
            point = new THREE.Vector3(0.0, cosinus, sinus);
            break;
          case 'y':
            point = new THREE.Vector3(cosinus, 0.0, sinus);
            break;
          case 'z':
            point = new THREE.Vector3(sinus, cosinus, 0.0);
            break;
        }
        geometry.vertices.push(point.multiplyScalar(radius));
      }
      return geometry;
    };

    mesh = new THREE.Line(new Circle(1, 'x', 0.0, 0.5), new LineMaterial(red));
    mesh.name = 'RX';
    displayAxes['rotate'].add(mesh);
    mesh = new THREE.LineSegments(
      new Circle(1.0, 'x', 0.5, 1.0),
      new LineMaterial(red)
    );
    mesh.name = 'RXB';
    displayAxes['rotate'].add(mesh);

    mesh = new THREE.Line(
      new Circle(1, 'y', 0.0, 0.5),
      new LineMaterial(green)
    );
    mesh.name = 'RY';
    displayAxes['rotate'].add(mesh);
    mesh = new THREE.LineSegments(
      new Circle(1, 'y', 0.5, 1.0),
      new LineMaterial(green)
    );
    mesh.name = 'RYB';
    displayAxes['rotate'].add(mesh);

    mesh = new THREE.Line(new Circle(1, 'z', 0.0, 0.5), new LineMaterial(blue));
    mesh.name = 'RZ';
    displayAxes['rotate'].add(mesh);
    mesh = new THREE.LineSegments(
      new Circle(1, 'z', 0.5, 1.0),
      new LineMaterial(blue)
    );
    mesh.name = 'RZB';
    displayAxes['rotate'].add(mesh);

    mesh = new THREE.Line(
      new Circle(1.25, 'z'),
      new LineMaterial(yellow, 0.25)
    );
    mesh.name = 'RE';
    this.pickerNames.push(mesh.name);
    displayAxes['rotate'].add(mesh);

    // Picker spheres
    mesh = new THREE.Mesh(
      new THREE.SphereGeometry(0.95, 12, 12),
      new HandleMaterial(white, 0.25)
    );
    mesh.name = 'RXYZE';
    pickerAxes['rotate'].add(mesh);
    this.pickerNames.push(mesh.name);

    intersectionPlanes['SPHERE'] = new THREE.Mesh(
      new THREE.SphereGeometry(0.95, 12, 12)
    );
    intersectionPlanes['SPHERE'].visible = false;
    planes.add(intersectionPlanes['SPHERE']);

    mesh = new THREE.Mesh(
      new THREE.TorusGeometry(1.3, 0.15, 4, 12),
      new HandleMaterial(yellow, 0.25)
    );
    mesh.name = 'RE';
    pickerAxes['rotate'].add(mesh);
    this.pickerNames.push(mesh.name);
  }
  mesh = null;

  /**
   * Attach gizmo to an object
   * @param {THREE.Object3D} - Model to be manipulated
   */
  this.attach = function(object) {
    this.object = object;
    this.setMode(scope.mode);

    if (this.mobile) {
      this.userView.container.addEventListener(
        'touchstart',
        onTouchStart,
        false
      );
    } else {
      this.userView.container.addEventListener('mousedown', onMouseDown, false);
      this.userView.container.addEventListener(
        'mousemove',
        onMouseHover,
        false
      );
    }
  };

  /**
   * Detatch gizmo from an object
   * @param {THREE.Object3D} - Model
   */
  this.detach = function(object) {
    this.object = undefined;
    this.selected = 'null';

    this.hide();

    if (this.mobile) {
      this.userView.container.removeEventListener(
        'touchstart',
        onTouchStart,
        false
      );
    } else {
      this.userView.container.removeEventListener(
        'mousedown',
        onMouseDown,
        false
      );
      this.userView.container.removeEventListener(
        'mousemove',
        onMouseHover,
        false
      );
    }
  };

  /**
   * Update gizmo's pose and scale
   */
  this.update = function() {
    if (this.gz3dScene.manipulationMode !== 'natural') {
      setMoveHoverMesh(null);
    }

    if (this.object === undefined) {
      return;
    }

    this.object.updateMatrixWorld();
    this.gz3dScene.updateBoundingBox(this.object);

    worldPosition.setFromMatrixPosition(this.object.matrixWorld);

    this.userView.camera.updateMatrixWorld();
    camPosition.setFromMatrixPosition(this.userView.camera.matrixWorld);

    scale = worldPosition.distanceTo(camPosition) / 6 * this.scale;
    this.gizmo.position.copy(worldPosition);
    this.gizmo.scale.set(scale, scale, scale);

    for (var i in this.gizmo.children) {
      for (var j in this.gizmo.children[i].children) {
        var object = this.gizmo.children[i].children[j];
        var name = object.name;

        if (name.search('E') !== -1) {
          lookAtMatrix.lookAt(
            camPosition,
            worldPosition,
            tempVector.set(0, 1, 0)
          );
          object.rotation.setFromRotationMatrix(lookAtMatrix);
        } else {
          eye
            .copy(camPosition)
            .sub(worldPosition)
            .normalize();

          if (this.space === 'local') {
            tempQuaternion.setFromRotationMatrix(
              tempMatrix.extractRotation(this.object.matrixWorld)
            );

            if (name.search('R') !== -1) {
              tempMatrix
                .makeRotationFromQuaternion(tempQuaternion)
                .getInverse(tempMatrix);
              eye.applyMatrix4(tempMatrix);
              switch (name) {
                case 'RX':
                case 'RXB':
                  quaternionX.setFromAxisAngle(
                    unitX,
                    Math.atan2(-eye.y, eye.z)
                  );
                  tempQuaternion.multiplyQuaternions(
                    tempQuaternion,
                    quaternionX
                  );
                  break;
                case 'RY':
                case 'RYB':
                  quaternionY.setFromAxisAngle(unitY, Math.atan2(eye.x, eye.z));
                  tempQuaternion.multiplyQuaternions(
                    tempQuaternion,
                    quaternionY
                  );
                  break;
                case 'RZ':
                case 'RZB':
                  quaternionZ.setFromAxisAngle(unitZ, Math.atan2(eye.y, eye.x));
                  tempQuaternion.multiplyQuaternions(
                    tempQuaternion,
                    quaternionZ
                  );
                  break;
              }
            }
            object.quaternion.copy(tempQuaternion);
          } else if (this.space === 'world') {
            object.rotation.set(0, 0, 0);
            switch (name) {
              case 'RX':
              case 'RXB':
                object.rotation.x = Math.atan2(-eye.y, eye.z);
                break;
              case 'RY':
              case 'RYB':
                object.rotation.y = Math.atan2(eye.x, eye.z);
                break;
              case 'RZ':
              case 'RZB':
                object.rotation.z = Math.atan2(eye.y, eye.x);
                break;
            }
          }
        }
      }
    }

    scope.gz3dScene.refresh3DViews();
  };

  /**
   * Hide gizmo
   */
  this.hide = function() {
    for (var i in displayAxes) {
      for (var j in displayAxes[i].children) {
        displayAxes[i].children[j].visible = false;
      }
    }
    for (var k in pickerAxes) {
      for (var l in pickerAxes[k].children) {
        pickerAxes[k].children[l].visible = false;
      }
    }

    for (var m in intersectionPlaneList) {
      intersectionPlanes[intersectionPlaneList[m]].visible = false;
    }
  };

  /**
   * Set mode
   * @param {string} value - translate | scale | rotate
   */
  this.setMode = function(value) {
    scope.mode = value;

    this.hide();

    for (var i in displayAxes[this.mode].children) {
      displayAxes[this.mode].children[i].visible = true;
    }

    for (var j in pickerAxes[this.mode].children) {
      pickerAxes[this.mode].children[j].visible = false; // debug
    }

    for (var k in intersectionPlaneList) {
      intersectionPlanes[intersectionPlaneList[k]].visible = false; // debug
    }

    scope.update();
  };

  /**
   * Choose intersection plane
   */
  this.setIntersectionPlane = function() {
    eye
      .copy(camPosition)
      .sub(worldPosition)
      .normalize();

    if (this.space === 'local') {
      eye.applyMatrix4(tempMatrix.getInverse(scope.object.matrixWorld));
    }

    var T = GZ3D.TRANSFORM_TYPE_NAME_PREFIX.TRANSLATE;
    var S = GZ3D.TRANSFORM_TYPE_NAME_PREFIX.SCALE;

    if (isSelected(T + 'XYZ') || isSelected(S + 'XYZ')) {
      if (
        Math.abs(eye.x) > Math.abs(eye.y) &&
        Math.abs(eye.x) > Math.abs(eye.z)
      ) {
        currentPlane = 'YZ';
      } else if (
        Math.abs(eye.y) > Math.abs(eye.x) &&
        Math.abs(eye.y) > Math.abs(eye.z)
      ) {
        currentPlane = 'XZ';
      } else {
        currentPlane = 'XY';
      }
    } else if (
      isSelected('RX') ||
      isSelected(T + 'YZ') ||
      isSelected(S + 'YZ')
    ) {
      currentPlane = 'YZ';
    } else if (
      isSelected('RY') ||
      isSelected(T + 'XZ') ||
      isSelected(S + 'XZ')
    ) {
      currentPlane = 'XZ';
    } else if (
      isSelected('RZ') ||
      isSelected(T + 'XY') ||
      isSelected(S + 'XY')
    ) {
      currentPlane = 'XY';
    } else if (isSelected('X')) {
      if (Math.abs(eye.y) > Math.abs(eye.z)) {
        currentPlane = 'XZ';
      } else {
        currentPlane = 'XY';
      }
    } else if (isSelected('Y')) {
      if (Math.abs(eye.x) > Math.abs(eye.z)) {
        currentPlane = 'YZ';
      } else {
        currentPlane = 'XY';
      }
    } else if (isSelected('Z')) {
      if (Math.abs(eye.x) > Math.abs(eye.y)) {
        currentPlane = 'YZ';
      } else {
        currentPlane = 'XZ';
      }
    }
  };

  /**
   * Window event callback
   * @param {} event
   */
  function onTouchStart(event) {
    event.preventDefault();

    var intersect = intersectObjects(event, pickerAxes[scope.mode].children);

    // If one of the current pickers was touched
    if (intersect) {
      if (scope.gz3dScene.controls) {
        scope.gz3dScene.controls.enabled = false;
      }

      if (selectedPicker !== intersect.object) {
        // Back to original color
        if (selectedPicker !== null) {
          selectedPicker.material.color.copy(selectedColor);
        }

        selectedPicker = intersect.object;

        // Save color for when it's deselected
        selectedColor.copy(selectedPicker.material.color);

        // Darken color
        selectedPicker.material.color.offsetHSL(0, 0, -0.3);

        scope.dispatchEvent(changeEvent());
      }

      scope.selected = selectedPicker.name;
      scope.hovered = true;
      scope.update();
      scope.setIntersectionPlane();

      var planeIntersect = intersectObjects(event, [
        intersectionPlanes[currentPlane]
      ]);

      if (planeIntersect) {
        oldPosition.copy(scope.object.position);

        oldRotationMatrix.extractRotation(scope.object.matrix);
        worldRotationMatrix.extractRotation(scope.object.matrixWorld);

        parentRotationMatrix.extractRotation(scope.object.parent.matrixWorld);
        parentScale.setFromMatrixScale(
          tempMatrix.getInverse(scope.object.parent.matrixWorld)
        );

        offset.copy(planeIntersect.point);
      }
    }

    scope.userView.container.addEventListener(
      'touchmove',
      onPointerMove,
      false
    );
    scope.userView.container.addEventListener('touchend', onTouchEnd, false);
  }

  /**
   * Window event callback
   * @param {} event
   */
  function onTouchEnd() {
    // Previously selected picker back to its color
    if (selectedPicker) {
      selectedPicker.material.color.copy(selectedColor);
    }

    selectedPicker = null;

    scope.dispatchEvent(changeEvent());

    scope.selected = 'null';
    scope.hovered = false;

    scope.userView.container.removeEventListener(
      'touchmove',
      onPointerMove,
      false
    );
    scope.userView.container.removeEventListener('touchend', onTouchEnd, false);

    if (scope.gz3dScene.controls) {
      scope.gz3dScene.controls.enabled = true;
    }
  }

  this.highlightPicker = function(picker) {
    highlightPicker(picker);
  };

  function highlightPicker(picker) {
    if (picker) {
      if (hovered !== picker) {
        if (hovered !== null) {
          hovered.material.color.copy(hoveredColor);
        }

        selectedPicker = picker;
        hovered = picker;
        hoveredColor.copy(hovered.material.color);

        hovered.material.color.offsetHSL(0, 0, -0.3);

        scope.dispatchEvent(changeEvent());
      }
      scope.hovered = true;
    } else if (hovered !== null) {
      hovered.material.color.copy(hoveredColor);

      hovered = null;

      scope.dispatchEvent(changeEvent());

      scope.hovered = false;
    }
  }

  /**
   * Window event callback
   * @param {} event
   */
  function onMouseHover(event) {
    event.preventDefault();

    if (event.button === 0 && scope.selected === 'null') {
      var intersect = intersectObjects(event, pickerAxes[scope.mode].children);
      highlightPicker(intersect.object);
    }
    scope.userView.container.addEventListener(
      'mousemove',
      onPointerMove,
      false
    );
    scope.userView.container.addEventListener('mouseup', onMouseUp, false);
  }

  this.selectPicker = function(event) {
    selectPicker(event);
  };

  function selectPicker(event) {
    scope.selected = selectedPicker.name;

    scope.update();
    scope.setIntersectionPlane();

    var planeIntersect = intersectObjects(event, [
      intersectionPlanes[currentPlane]
    ]);

    var rect = scope.userView.container.getBoundingClientRect();
    var x = (event.clientX - rect.left) / rect.width;
    var y = (event.clientY - rect.top) / rect.height;

    startRay.copy(ray.ray); // copy the ray used to raycast from the start point

    if (planeIntersect) {
      oldPosition.copy(scope.object.position);
      oldScale.copy(scope.object.scale);

      oldRotationMatrix.extractRotation(scope.object.matrix);
      worldRotationMatrix.extractRotation(scope.object.matrixWorld);

      parentRotationMatrix.extractRotation(scope.object.parent.matrixWorld);
      parentScale.setFromMatrixScale(
        tempMatrix.getInverse(scope.object.parent.matrixWorld)
      );

      offset.copy(planeIntersect.point);
    }
  }

  /**
   * Window event callback
   * @param {} event
   */
  function onMouseDown(event) {
    event.preventDefault();

    if (event.button !== 0) {
      return;
    }

    var intersect = intersectObjects(event, pickerAxes[scope.mode].children);

    if (intersect) {
      selectPicker(event);

      if (scope.gz3dScene.controls) {
        scope.gz3dScene.controls.enabled = false;
      }
    }

    scope.userView.container.addEventListener(
      'mousemove',
      onPointerMove,
      false
    );
    scope.userView.container.addEventListener('mouseup', onMouseUp, false);
  }

  this.onPointerMove = function(event) {
    onPointerMove(event);
  };

  var moveHoverMesh = null;

  function setMoveHoverMesh(obj) {
    if (moveHoverMesh === obj) {
      return;
    }

    if (moveHoverMesh) {
      scope.gz3dScene.hideBoundingBox();
    }

    if (obj) {
      scope.gz3dScene.showBoundingBox(obj);
    }

    moveHoverMesh = obj;
    scope.gz3dScene.dontRebuildCubemapNow = moveHoverMesh !== null;
    scope.gz3dScene.refresh3DViews();
  }

  function processNaturalMoveHover(event) {
    if (scope.gz3dScene.naturalAutoAlignMode) {
      return;
    }

    var positionX = event.offsetX,
      positionY = event.offsetY;
    var raycaster = new THREE.Raycaster();
    var i;
    var distance = -1000;
    var hitDesc;
    var point;

    var viewWidth = scope.gz3dScene.getDomElement().clientWidth;
    var viewHeight = scope.gz3dScene.getDomElement().clientHeight;

    raycaster.setFromCamera(
      new THREE.Vector2(
        positionX / viewWidth * 2 - 1,
        -(positionY / viewHeight) * 2 + 1
      ),
      scope.gz3dScene.camera
    );

    var objects = raycaster.intersectObjects(
      scope.gz3dScene.scene.children,
      true
    );

    for (i = 0; i < objects.length; i++) {
      var idesc = objects[i];
      var meshobj = idesc.object;

      if (
        meshobj.name.indexOf('_lightHelper') >= 0 ||
        meshobj.name === 'grid' ||
        meshobj.name === 'boundingBox' ||
        meshobj instanceof THREE.Camera ||
        meshobj instanceof THREE.Light ||
        meshobj instanceof THREE.CameraHelper ||
        meshobj instanceof THREE.Lensflare ||
        meshobj instanceof THREE.GridHelper
      ) {
        continue;
      }

      if (idesc.distance < distance || !point) {
        distance = idesc.distance;
        hitDesc = idesc;
        point = idesc.point.clone();
      }
    }

    if (hitDesc && hitDesc.object) {
      var rootObject = hitDesc.object;
      while (rootObject && !(rootObject.parent instanceof THREE.Scene)) {
        rootObject = rootObject.parent;
      }

      setMoveHoverMesh(rootObject);
    } else {
      setMoveHoverMesh(null);
    }
  }

  /**
   * Window event callback (mouse move and touch move)
   * @param {} event
   */
  function onPointerMove(event) {
    if (scope.gz3dScene.naturalAutoAlignMode) {
      setMoveHoverMesh(scope.gz3dScene.naturalAutoAlignMode.obj);
    } else if (scope.gz3dScene.manipulationMode === 'natural') {
      processNaturalMoveHover(event);
    } else {
      setMoveHoverMesh(null);
    }

    if (scope.selected === 'null') {
      return;
    }

    event.preventDefault();

    var planeIntersect = intersectObjects(event, [
      intersectionPlanes[currentPlane]
    ]);

    var rect = scope.userView.container.getBoundingClientRect();
    var x = (event.clientX - rect.left) / rect.width;
    var y = (event.clientY - rect.top) / rect.height;

    lastRay.copy(ray.ray); // copy the ray used to raycast from the end point

    var transformType;

    if (planeIntersect) {
      var initPosition = Object.assign({}, scope.object.position);
      var currentObject = scope.object;
      point.copy(planeIntersect.point);

      if (
        scope.mode === 'translate' &&
        isSelected(GZ3D.TRANSFORM_TYPE_NAME_PREFIX.TRANSLATE)
      ) {
        point.sub(offset);
        point.multiply(parentScale);

        if (scope.space === 'local') {
          point.applyMatrix4(tempMatrix.getInverse(worldRotationMatrix));

          if (!isSelected('X') || scope.modifierAxis.x !== 1) {
            point.x = 0;
          }
          if (!isSelected('Y') || scope.modifierAxis.y !== 1) {
            point.y = 0;
          }
          if (!isSelected('Z') || scope.modifierAxis.z !== 1) {
            point.z = 0;
          }
          if (isSelected('XYZ')) {
            point.set(0, 0, 0);
          }
          point.applyMatrix4(oldRotationMatrix);

          scope.object.position.copy(oldPosition);
          scope.object.position.add(point);
        }
        if (scope.space === 'world' || isSelected('XYZ')) {
          if (!isSelected('X') || scope.modifierAxis.x !== 1) {
            point.x = 0;
          }
          if (!isSelected('Y') || scope.modifierAxis.y !== 1) {
            point.y = 0;
          }
          if (!isSelected('Z') || scope.modifierAxis.z !== 1) {
            point.z = 0;
          }

          point.applyMatrix4(tempMatrix.getInverse(parentRotationMatrix));

          currentObject.position.copy(oldPosition);
          currentObject.position.add(point);

          if (scope.snapDist) {
            if (isSelected('X')) {
              currentObject.position.x =
                Math.round(currentObject.position.x / scope.snapDist) *
                scope.snapDist;
            }
            if (isSelected('Y')) {
              currentObject.position.y =
                Math.round(currentObject.position.y / scope.snapDist) *
                scope.snapDist;
            }
            if (isSelected('Z')) {
              currentObject.position.z =
                Math.round(currentObject.position.z / scope.snapDist) *
                scope.snapDist;
            }
          }
        }
        // workaround for the problem when an object jumps straight to (0,0,0) on translation
        if (
          scope.object.position.x === 0 &&
          scope.object.position.y === 0 &&
          scope.object.position.z === 0
        ) {
          // a "jump" detected -> keep the object at initial position
          scope.object.position.copy(initPosition);
          selectPicker(event);
        }

        transformType = GZ3D.TRANSFORM_TYPE_NAME_PREFIX.TRANSLATE;
      } else if (
        scope.mode === 'scale' &&
        isSelected(GZ3D.TRANSFORM_TYPE_NAME_PREFIX.SCALE)
      ) {
        if (!currentObject.isSimpleShape()) {
          return;
        }

        var distanceToPlane = function(plane, origin, dir) {
          var denom = plane.normal.dot(dir);
          if (Math.abs(denom) < 1e-3) {
            return 0;
          } else {
            var nom = origin.dot(plane.normal) - plane.constant;
            return -(nom / denom);
          }
        };

        // adapted from gazebo::gui:ModelManipulator
        var axis = new THREE.Vector3();

        if (isSelected('X')) {
          axis.x = 1;
        }
        if (isSelected('Y')) {
          axis.y = 1;
        }
        if (isSelected('Z')) {
          axis.z = 1;
        }

        var getMouseMoveDistance = function(
          startRay,
          endRay,
          pose,
          axis,
          isLocal
        ) {
          var origin1 = startRay.origin;
          var direction1 = startRay.direction;

          var origin2 = endRay.origin;
          var direction2 = endRay.direction;

          var planeNorm = new THREE.Vector3(0, 0, 0);
          var projNorm = new THREE.Vector3(0, 0, 0);

          var planeNormOther = new THREE.Vector3(0, 0, 0);

          if (axis.x > 0 && axis.y > 0) {
            planeNorm.z = 1;
            projNorm.z = 1;
          } else if (axis.z > 0) {
            planeNorm.y = 1;
            projNorm.x = 1;
            planeNormOther.x = 1;
          } else if (axis.x > 0) {
            planeNorm.z = 1;
            projNorm.y = 1;
            planeNormOther.y = 1;
          } else if (axis.y > 0) {
            planeNorm.z = 1;
            projNorm.x = 1;
            planeNormOther.x = 1;
          }

          if (isLocal) {
            planeNorm.applyQuaternion(pose.rot);
            projNorm.applyQuaternion(pose.rot);
          }

          // Fine tune ray casting: cast a second ray and compare the two rays' angle
          // to plane. Use the one that is less parallel to plane for better results.
          var angle = direction1.dot(planeNorm);
          if (isLocal) {
            planeNormOther.applyQuaternion(pose.rot);
          }
          var angleOther = direction1.dot(planeNormOther);
          if (Math.abs(angleOther) > Math.abs(angle)) {
            projNorm = planeNorm;
            planeNorm = planeNormOther;
          }

          // Compute the distance from the camera to plane
          var d = pose.pos.dot(planeNorm);
          var plane = new THREE.Plane(planeNorm, d);

          var dist1 = distanceToPlane(plane, origin1, direction1);
          var dist2 = distanceToPlane(plane, origin2, direction1);
          var tmpVector = new THREE.Vector3();

          // Compute two points on the plane. The first point is the current
          // mouse position, the second is the previous mouse position
          tmpVector.copy(direction1).multiplyScalar(dist1); // dir1 * dist1
          var p1 = new THREE.Vector3().addVectors(origin1, tmpVector); //origin1 + (dir1 * dist1)

          tmpVector.copy(direction2).multiplyScalar(dist2);
          var p2 = new THREE.Vector3().addVectors(origin2, tmpVector);

          if (isLocal) {
            var p1MinusP2 = tmpVector;

            p1MinusP2.subVectors(p1, p2); //(p1-p2)

            // p1 = p1 - (projNorm * (p1-p2).Dot(projNorm));
            p1.sub(projNorm.multiplyScalar(p1MinusP2.dot(projNorm)));
          }

          var distance = tmpVector.subVectors(p1, p2);

          if (!isLocal) {
            distance.multiply(axis);
          }

          return distance;
        };

        var updateScale = function(scaleVector, axis, shape) {
          // impose uniform transformation constraints on simple shapes

          switch (shape) {
            case 'cylinder': //X=Y Z
              if (axis.x > 0) {
                scaleVector.y = scaleVector.x;
              } else if (axis.y > 0) {
                scaleVector.x = scaleVector.y;
              }
              break;
            case 'sphere': //X=Y=Z
              if (axis.x > 0) {
                scaleVector.y = scaleVector.z = scaleVector.x;
              } else if (axis.y > 0) {
                scaleVector.x = scaleVector.z = scaleVector.y;
              } else if (axis.z > 0) {
                scaleVector.x = scaleVector.y = scaleVector.z;
              }
              break;
            case undefined: // undefined --> do nothing
              break;
            // box and complex --> do nothing
          }
        };

        let worldPose = new THREE.Vector3();
        let worldRotation = new THREE.Quaternion();
        currentObject.getWorldPosition(worldPose);
        currentObject.getWorldQuaternion(worldRotation);
        var objectWorldPose = {
          pos: worldPose,
          rot: worldRotation
        };

        var mouseDistance = getMouseMoveDistance(
          startRay,
          lastRay,
          objectWorldPose,
          axis,
          scope.space === 'local'
        );

        mouseDistance.multiply(axis); //filter out unselected component. i.e. keep only the coordinates set to 1 in axis.

        var objectWorldInverseRotation = objectWorldPose.rot.clone().inverse();
        mouseDistance.applyQuaternion(objectWorldInverseRotation);

        var scale = new THREE.Vector3(1, 1, 1).sub(mouseDistance); // 1 - mouseDistance

        //check simple shapes constraints
        updateScale(scale, axis, currentObject.getShapeName());

        var absScale = new THREE.Vector3(
          Math.abs(scale.x),
          Math.abs(scale.y),
          Math.abs(scale.z)
        );
        var newScale = absScale.multiply(oldScale);

        currentObject.scale.copy(newScale);

        transformType = GZ3D.TRANSFORM_TYPE_NAME_PREFIX.SCALE;
      } else if (
        scope.mode === 'rotate' &&
        isSelected(GZ3D.TRANSFORM_TYPE_NAME_PREFIX.ROTATE)
      ) {
        point.sub(worldPosition);
        point.multiply(parentScale);
        tempVector.copy(offset).sub(worldPosition);
        tempVector.multiply(parentScale);

        if (scope.selected === 'RE') {
          point.applyMatrix4(tempMatrix.getInverse(lookAtMatrix));
          tempVector.applyMatrix4(tempMatrix.getInverse(lookAtMatrix));

          rotation.set(
            Math.atan2(point.z, point.y),
            Math.atan2(point.x, point.z),
            Math.atan2(point.y, point.x)
          );
          offsetRotation.set(
            Math.atan2(tempVector.z, tempVector.y),
            Math.atan2(tempVector.x, tempVector.z),
            Math.atan2(tempVector.y, tempVector.x)
          );

          tempQuaternion.setFromRotationMatrix(
            tempMatrix.getInverse(parentRotationMatrix)
          );

          quaternionE.setFromAxisAngle(eye, rotation.z - offsetRotation.z);
          quaternionXYZ.setFromRotationMatrix(worldRotationMatrix);

          tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionE);
          tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionXYZ);

          currentObject.quaternion.copy(tempQuaternion);
        } else if (scope.selected === 'RXYZE') {
          // commented out for now since it throws an error message at quaternionE.setFromEuler('not a THREE.Euler')
          // and has unexpected results when actually followed through with a THREE.Euler passed to setFromEuler()
          /*
          quaternionE.setFromEuler(point.clone().cross(tempVector).normalize()); // has this ever worked?

          tempQuaternion.setFromRotationMatrix(tempMatrix.getInverse(parentRotationMatrix));
          quaternionX.setFromAxisAngle(quaternionE, - point.clone().angleTo(tempVector));
          quaternionXYZ.setFromRotationMatrix(worldRotationMatrix);

          tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionX);
          tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionXYZ);

          currentObject.quaternion.copy(tempQuaternion);
          */
        } else {
          if (scope.space === 'local') {
            point.applyMatrix4(tempMatrix.getInverse(worldRotationMatrix));

            tempVector.applyMatrix4(tempMatrix.getInverse(worldRotationMatrix));

            rotation.set(
              Math.atan2(point.z, point.y),
              Math.atan2(point.x, point.z),
              Math.atan2(point.y, point.x)
            );
            offsetRotation.set(
              Math.atan2(tempVector.z, tempVector.y),
              Math.atan2(tempVector.x, tempVector.z),
              Math.atan2(tempVector.y, tempVector.x)
            );

            quaternionXYZ.setFromRotationMatrix(oldRotationMatrix);
            quaternionX.setFromAxisAngle(unitX, rotation.x - offsetRotation.x);
            quaternionY.setFromAxisAngle(unitY, rotation.y - offsetRotation.y);
            quaternionZ.setFromAxisAngle(unitZ, rotation.z - offsetRotation.z);

            if (scope.selected === 'RX') {
              quaternionXYZ.multiplyQuaternions(quaternionXYZ, quaternionX);
            }
            if (scope.selected === 'RY') {
              quaternionXYZ.multiplyQuaternions(quaternionXYZ, quaternionY);
            }
            if (scope.selected === 'RZ') {
              quaternionXYZ.multiplyQuaternions(quaternionXYZ, quaternionZ);
            }

            currentObject.quaternion.copy(quaternionXYZ);
          } else if (scope.space === 'world') {
            rotation.set(
              Math.atan2(point.z, point.y),
              Math.atan2(point.x, point.z),
              Math.atan2(point.y, point.x)
            );
            offsetRotation.set(
              Math.atan2(tempVector.z, tempVector.y),
              Math.atan2(tempVector.x, tempVector.z),
              Math.atan2(tempVector.y, tempVector.x)
            );

            tempQuaternion.setFromRotationMatrix(
              tempMatrix.getInverse(parentRotationMatrix)
            );

            quaternionX.setFromAxisAngle(unitX, rotation.x - offsetRotation.x);
            quaternionY.setFromAxisAngle(unitY, rotation.y - offsetRotation.y);
            quaternionZ.setFromAxisAngle(unitZ, rotation.z - offsetRotation.z);
            quaternionXYZ.setFromRotationMatrix(worldRotationMatrix);

            if (scope.selected === 'RX') {
              tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionX);
            }
            if (scope.selected === 'RY') {
              tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionY);
            }
            if (scope.selected === 'RZ') {
              tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionZ);
            }

            tempQuaternion.multiplyQuaternions(tempQuaternion, quaternionXYZ);

            currentObject.quaternion.copy(tempQuaternion);
          }
        }

        transformType = GZ3D.TRANSFORM_TYPE_NAME_PREFIX.ROTATE;
      }
    }

    scope.update();
    scope.dispatchEvent(changeEvent(transformType));
  }

  this.handleAxisLockEnd = function() {
    handleAxisLockEnd();
  };

  function handleAxisLockEnd() {
    highlightPicker();
    scope.selected = 'null';

    scope.userView.container.removeEventListener(
      'mousemove',
      onPointerMove,
      false
    );
    scope.userView.container.removeEventListener('mouseup', onMouseUp, false);

    scope.gz3dScene.updateDynamicEnvMap();
  }

  function onMouseUp(event) {
    handleAxisLockEnd();
    if (scope.gz3dScene.controls) {
      scope.gz3dScene.controls.enabled = true;
    }
  }

  /**
   * intersectObjects
   * @param {} event
   * @param {} objects
   * @returns {?}
   */
  function intersectObjects(event, objects) {
    var pointer = event.touches ? event.touches[0] : event;

    var rect = scope.userView.container.getBoundingClientRect();
    var x = (pointer.clientX - rect.left) / rect.width;
    var y = (pointer.clientY - rect.top) / rect.height;
    var i;

    pointerVector.set(x * 2 - 1, -y * 2 + 1, 0.5);

    pointerVector.unproject(scope.userView.camera);
    ray.set(camPosition, pointerVector.sub(camPosition).normalize());

    for (i = 0; i < objects.length; i++) {
      objects[i].visible = true;
    } // ThreeJS intersectObjects function requires object to be visible

    // checks all intersections between the ray and the objects,
    // true to check the descendants
    var intersections = ray.intersectObjects(objects, true);

    for (i = 0; i < objects.length; i++) {
      objects[i].visible = false;
    } // Hide them again after the test

    return intersections[0] ? intersections[0] : false;
  }

  this.isSelected = function(name) {
    return isSelected(name);
  };
  /**
   * Checks if given name is currently selected
   * @param {} name
   * @returns {boolean}
   */
  function isSelected(name) {
    return scope.selected.search(name) !== -1;
  }

  /**
   * bakeTransformations
   * @param {} object
   */
  function bakeTransformations(object) {
    var tempGeometry = new THREE.Geometry();
    object.updateMatrix();
    tempGeometry.merge(object.geometry, object.matrix);
    object.geometry = tempGeometry;
    object.position.set(0, 0, 0);
    object.rotation.set(0, 0, 0);
    object.scale.set(1, 1, 1);
  }
};

GZ3D.Manipulator.prototype = Object.create(THREE.EventDispatcher.prototype);
