GZ3D.VisualFluidModel = function(scene) {
  this.scene = scene;
  this.rootGroup = new THREE.Group();
  this.rootGroup.name = 'VisualFluidModel';
  scene.scene.add(this.rootGroup);
  this.rootGroup.visible = true;
};

GZ3D.VisualFluidModel.prototype.initFluidBuffers = function(message) {
  this.scales = new Float32Array(message.position.length);
  this.scales.fill(10.0);
  this.positions = new Float32Array(message.position.length * 3);
  this.geometry = new THREE.BufferGeometry();
  this.positionAttribute = new THREE.BufferAttribute(this.positions, 3);
  this.positionAttribute.setDynamic(true);
  this.geometry.addAttribute('position', this.positionAttribute);
  this.geometry.addAttribute(
    'scale',
    new THREE.BufferAttribute(this.scales, 1)
  );
  this.material = new THREE.ParticleBasicMaterial({
    size: 5.0,
    sizeAttenuation: false,
    color: 0x2389da
  });
  this.particles = new THREE.Points(this.geometry, this.material);
  if (this.rootGroup.children) {
    delete this.rootGroup.children;
  }
  this.rootGroup.children = [];
  this.rootGroup.add(this.particles);
};

GZ3D.VisualFluidModel.prototype.updateVisualization = function(message) {
  var vertex;
  for (var i = 0, l = message.position.length; i < l; i++) {
    vertex = new THREE.Vector3(
      message.position[i].x,
      message.position[i].y,
      message.position[i].z
    );
    vertex.toArray(this.positions, i * 3);
  }
  this.positionAttribute.needsUpdate = true;
};
