/**
 * HBP
 *
 * Composer settings. This is where you specify global post-processing settings
 * such as ssao, etc. Composer settings can be accessed in scene.composerSettings.
 * Once modified you need to call scene.applyComposerSettings to reflect the changes
 * in the 3D scene.
 */

// Master settings

GZ3D.MASTER_QUALITY_BEST = 'Best';
GZ3D.MASTER_QUALITY_MIDDLE = 'Middle';
GZ3D.MASTER_QUALITY_LOW = 'Low';
GZ3D.MASTER_QUALITY_MINIMAL = 'Minimal';

// Composer Settings

GZ3D.ComposerSettings = function() {
  this.shadows = false;
  this.antiAliasing = true;

  this.ssao = false; // Screen space ambient occlusion
  this.ssaoDisplay = false;
  this.ssaoClamp = 0.8;
  this.ssaoLumInfluence = 0.7;

  this.rgbCurve = { red: [], green: [], blue: [] }; // Color correction, disabled by default

  this.levelsInBlack = 0.0; // Color levels
  this.levelsInGamma = 1.0;
  this.levelsInWhite = 1.0;
  this.levelsOutBlack = 0.0;
  this.levelsOutWhite = 1.0;

  this.skyBox = ''; // The file path of the sky box (without file extension) or empty string for plain color background
  this.dynamicEnvMap = true; // Environment map is generated at the beginning of the experiment

  this.sun = ''; // empty string for no sun or "SIMPLELENSFLARE" for simple lens flare rendering

  this.bloom = false; // Bloom
  this.bloomStrength = 1.0;
  this.bloomRadius = 0.37;
  this.bloomThreshold = 0.98;

  this.fog = false; // Fog
  this.fogDensity = 0.05;
  this.fogColor = '#b2b2b2'; // CSS style

  this.pbrMaterial = true; // Physically based material

  // User camera frustum settings

  this.verticalFOV = 60.0;
  this.nearClippingDistance = 0.15;
  this.farClippingDistance = 100.0;
  this.showCameraHelper = false;
};
