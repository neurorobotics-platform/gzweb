GZ3D.VisualMuscleModel = function(scene, robotName) {
  this.scene = scene;
  this.cylinderShapes = {};
  this.robotName = robotName;
  this.last_muscle_count = -1;
  this.radius = 1;
  this.rootGroup = new THREE.Group();
  this.rootGroup.name = 'VisualMuscleModel';
  scene.scene.add(this.rootGroup);
  this.rootGroup.visible = scene.composer.isSkinVisibleForMuscle(this);
};

GZ3D.VisualMuscleModel.prototype.setVisible = function(visible) {
  this.rootGroup.visible = visible;
};

GZ3D.VisualMuscleModel.prototype.updateVisualization = function(message) {
  if (message.robot_name === this.robotName) {
    if (this.last_muscle_count !== message.muscle.length) {
      // Compute the radius as fraction of the average length of all cylinders.
      var total_length = 0;
      for (var k1 = 0; k1 < message.muscle.length; ++k1) {
        total_length += message.muscle[k1].length;
      }
      this.radius = 0.05 * total_length / message.muscle.length;
      this.last_muscle_count = message.muscle.length;
    }

    for (var k = 0; k < message.muscle.length; ++k) {
      // NOTE: The number of path segments can vary over time
      // because wrap objects cause local refinement of the initial path.
      var cylinder = null;
      var cq = 0;
      var cylinderEndPoint_1 = null;
      var cylinderEndPoint_2 = null;
      var radius = this.radius;
      var activation = message.muscle[k].activation;
      var color;
      if (message.muscle[k].isMuscle) {
        color = new THREE.Color(activation, 0, 1 - activation);
      } else {
        color = new THREE.Color(activation, 1 - activation, 0); // Path actuators are green.
      }
      var num_segments = message.muscle[k].pathPoint.length - 1;

      // In case we don't have enough muscles.
      // NOTE: The case were the number of muscles decreases is not handled.
      // It would result in superfluous visible geometry.
      if (!(k in this.cylinderShapes)) {
        this.cylinderShapes[k] = {
          cylinders: [],
          spheres: []
        };
      }
      let cylinders = this.cylinderShapes[k].cylinders;
      for (cq = 0; cq < num_segments; ++cq) {
        // Create new cylinder on demand.
        if (cylinders.length <= cq) {
          let edgeGeometry = new THREE.CylinderBufferGeometry(1, 1, 1, 8, 1);
          let cylinder = new THREE.Mesh(
            edgeGeometry,
            new THREE.MeshLambertMaterial({
              color: color,
              wireframe: false,
              flatShading: THREE.FlatShading
            })
          );
          this.rootGroup.add(cylinder);
          cylinders.push(cylinder);
        }

        let cylinder = cylinders[cq];
        cylinder.visible = true; // In case less segments were needed the last frame.
        // Align and scale the cylinder.
        cylinderEndPoint_1 = new THREE.Vector3(
          message.muscle[k].pathPoint[cq].x,
          message.muscle[k].pathPoint[cq].y,
          message.muscle[k].pathPoint[cq].z
        );
        cylinderEndPoint_2 = new THREE.Vector3(
          message.muscle[k].pathPoint[cq + 1].x,
          message.muscle[k].pathPoint[cq + 1].y,
          message.muscle[k].pathPoint[cq + 1].z
        );
        let cylinder_direction = new THREE.Vector3().subVectors(
          cylinderEndPoint_2,
          cylinderEndPoint_1
        );
        cylinder.material.color = color;
        cylinder.scale.y = cylinder_direction.length();
        cylinder.scale.x = radius;
        cylinder.scale.z = radius;
        cylinder_direction.normalize();
        cylinder.quaternion.setFromUnitVectors(
          new THREE.Vector3(0, 1, 0),
          cylinder_direction
        );
        cylinder.position.x = (cylinderEndPoint_2.x + cylinderEndPoint_1.x) / 2;
        cylinder.position.y = (cylinderEndPoint_2.y + cylinderEndPoint_1.y) / 2;
        cylinder.position.z = (cylinderEndPoint_2.z + cylinderEndPoint_1.z) / 2;
      } // end for path points
      // I don't bother tracking how many segments were needed last time.
      // Simply loop over all superfluous segments and turn them all off.
      for (; cq < cylinders.length; ++cq) {
        cylinders[cq].visible = false;
      }
      // Now the spheres!
      let spheres = this.cylinderShapes[k].spheres;
      for (cq = 0; cq < message.muscle[k].pathPoint.length; ++cq) {
        // Create one if needed.
        if (spheres.length <= cq) {
          let geom = new THREE.SphereBufferGeometry(1, 8, 8);
          let s = new THREE.Mesh(
            geom,
            new THREE.MeshLambertMaterial({
              color: color,
              wireframe: false,
              flatShading: THREE.FlatShading
            })
          );
          this.rootGroup.add(s);
          spheres.push(s);
        }
        // Put it at the right place with the right size.
        let sphere = spheres[cq];
        sphere.visible = true;
        sphere.material.color = color;
        let pos = new THREE.Vector3(
          message.muscle[k].pathPoint[cq].x,
          message.muscle[k].pathPoint[cq].y,
          message.muscle[k].pathPoint[cq].z
        );
        sphere.position.copy(pos);
        sphere.scale.x = radius;
        sphere.scale.y = radius;
        sphere.scale.z = radius;
      }
      // Turn off not needed ones.
      for (; cq < spheres.length; ++cq) {
        spheres[cq].visible = false;
      }
    } // end for message.muscle
  }
};
